## [0.10.4](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.10.3...v0.10.4) (2024-03-11)


### Bug Fixes

* Исправить ошибку с регистрацией ([9bfd700](https://gitlab.com/s3app-team/s3app/s3app-server/commit/9bfd700d69b1a6e2bcf06dacc5821e23361e4f16))

## [0.10.3](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.10.2...v0.10.3) (2024-02-02)


### Bug Fixes

* Повысить timeout для postgres ([221dbc7](https://gitlab.com/s3app-team/s3app/s3app-server/commit/221dbc76223b994ae805f487f4ce09d1a9542f64))

## [0.10.2](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.10.1...v0.10.2) (2024-01-23)


### Bug Fixes

* Повысить timeout для postgres ([5cd898c](https://gitlab.com/s3app-team/s3app/s3app-server/commit/5cd898c1dab9951c9b4930f3df13f8d342afc836))

## [0.10.1](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.10.0...v0.10.1) (2024-01-23)


### Bug Fixes

* Повысить timeout для postgres ([d04edaa](https://gitlab.com/s3app-team/s3app/s3app-server/commit/d04edaacbb9f6a9234d8c3001253a0a2962a6fe9))

# [0.10.0](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.9.3...v0.10.0) (2024-01-21)


### Bug Fixes

* docker sample ([29620a6](https://gitlab.com/s3app-team/s3app/s3app-server/commit/29620a6d717a5d6412344c7ff38b1e7031b9bac5))


### Features

* Интеграция с репозиторием лицензий ([a8b1ebe](https://gitlab.com/s3app-team/s3app/s3app-server/commit/a8b1ebeb001e68129a5079819ba07e38f50dcb1a))

## [0.9.3](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.9.2...v0.9.3) (2024-01-19)


### Bug Fixes

* bug fix ([3912450](https://gitlab.com/s3app-team/s3app/s3app-server/commit/391245033ead7b368a775d9fee07a8a655625d5c))

## [0.9.2](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.9.1...v0.9.2) (2024-01-19)


### Bug Fixes

* bug fix ([c8ab764](https://gitlab.com/s3app-team/s3app/s3app-server/commit/c8ab76480fb628d50d156c858ace93b20f838bfc))

## [0.9.1](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.9.0...v0.9.1) (2024-01-17)


### Bug Fixes

* bug fixes ([a8ad879](https://gitlab.com/s3app-team/s3app/s3app-server/commit/a8ad8794f45ae9650347782a7b4ab05a27e2caa2))

# [0.9.0](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.8.0...v0.9.0) (2024-01-16)


### Features

* Рефакторинг архитектуры ([27a25f6](https://gitlab.com/s3app-team/s3app/s3app-server/commit/27a25f62295556e6df8263e56ac371b471c4211f))

# [0.8.0](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.7.0...v0.8.0) (2024-01-15)


### Features

* Перевод БД на Postgres ([d3d3792](https://gitlab.com/s3app-team/s3app/s3app-server/commit/d3d3792baf1a627e44cdca45b7fee2da5a8719ed))

# [0.7.0](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.6.1...v0.7.0) (2024-01-13)


### Features

* Оптимизация ci ([340b646](https://gitlab.com/s3app-team/s3app/s3app-server/commit/340b6461875c9cc9000cc786fddc2fa8bad68148))

## [0.6.1](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.6.0...v0.6.1) (2023-12-27)


### Bug Fixes

* Ошибка в алгоритме авторизации ([ae279e5](https://gitlab.com/s3app-team/s3app/s3app-server/commit/ae279e56906682b780dfbc404c65c70dc0329255))

# [0.6.0](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.5.1...v0.6.0) (2023-12-27)


### Features

* Демонстрационная версия без регистрации ([030a736](https://gitlab.com/s3app-team/s3app/s3app-server/commit/030a73638e1ed1433557d07762068bfdbb4e1e59))

## [0.5.1](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.5.0...v0.5.1) (2023-12-13)


### Bug Fixes

* Ошибка отправки email ([611eb9b](https://gitlab.com/s3app-team/s3app/s3app-server/commit/611eb9b188f2f2ab6e975d0e3680fb82c88aece4))

# [0.5.0](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.4.2...v0.5.0) (2023-12-13)


### Features

* Добавить подтверждение email ([443b61d](https://gitlab.com/s3app-team/s3app/s3app-server/commit/443b61dc8d7eefedbb13852bf16183f4746e7330))

## [0.4.2](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.4.1...v0.4.2) (2023-12-03)


### Bug Fixes

* crypto is not defined ([c4316d1](https://gitlab.com/s3app-team/s3app/s3app-server/commit/c4316d118abf51d3d21d7ef93ead6a5651e92caa))

## [0.4.1](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.4.0...v0.4.1) (2023-11-14)


### Bug Fixes

* Fixed user return ([0922854](https://gitlab.com/s3app-team/s3app/s3app-server/commit/09228549f8cf1bda8aaa4ec4b84ea85f3ec1fde6))

# [0.4.0](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.3.0...v0.4.0) (2023-11-01)


### Features

* Публичная докеризация ([c88ba03](https://gitlab.com/s3app-team/s3app/s3app-server/commit/c88ba03976339407977fd20fbb68427788a329da))

# [0.3.0](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.2.7...v0.3.0) (2023-10-16)


### Features

* Made a demo that disables all mutations except login and registration ([0ebf313](https://gitlab.com/s3app-team/s3app/s3app-server/commit/0ebf3134f35151f387705174c27eba85b5dc10b2))

## [0.2.7](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.2.6...v0.2.7) (2023-09-26)


### Bug Fixes

* Returned old schema and old functions ([7f99e3a](https://gitlab.com/s3app-team/s3app/s3app-server/commit/7f99e3a8d94f8df6a18ed4682373f2b450f734a4))

## [0.2.6](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.2.5...v0.2.6) (2023-09-26)


### Bug Fixes

* Removed required fields ([16f163d](https://gitlab.com/s3app-team/s3app/s3app-server/commit/16f163d557b7054f271f8c814a41d197e3b9ce7d))

## [0.2.5](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.2.4...v0.2.5) (2023-09-26)


### Bug Fixes

* Graphql fix ([45ee693](https://gitlab.com/s3app-team/s3app/s3app-server/commit/45ee69302694615af18481c50a23eea85700a22c))

## [0.2.4](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.2.3...v0.2.4) (2023-09-26)


### Bug Fixes

* Circle was not returning stages. Fixed. ([8b5b93f](https://gitlab.com/s3app-team/s3app/s3app-server/commit/8b5b93fd6a44c459ea00ec7db6825c88d9db86bc))

## [0.2.3](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.2.2...v0.2.3) (2023-09-25)


### Bug Fixes

* When adding a circle, 4 stages are created - todo, inProgress, check, ready ([86bc2b1](https://gitlab.com/s3app-team/s3app/s3app-server/commit/86bc2b12525b9b258b1b158f7baf06baf6d1620d))

## [0.2.2](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.2.1...v0.2.2) (2023-09-15)


### Bug Fixes

* Date issues ([ec44bb4](https://gitlab.com/s3app-team/s3app/s3app-server/commit/ec44bb42a999a21bcefd6a1f1e522a5fe7c8e6b4))

## [0.2.1](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.2.0...v0.2.1) (2023-09-12)


### Bug Fixes

* Участники спринта ([8bc6337](https://gitlab.com/s3app-team/s3app/s3app-server/commit/8bc633711eab1f7ba99048037128ef338a933337))

# [0.2.0](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.1.5...v0.2.0) (2023-09-01)


### Bug Fixes

* Auth fix ([cf49b61](https://gitlab.com/s3app-team/s3app/s3app-server/commit/cf49b61d4b509f1b0e70fb9d1e27523914e4bba1))


### Features

* Добавление и удаление пользователей из домена ([af70cc8](https://gitlab.com/s3app-team/s3app/s3app-server/commit/af70cc8e10be53636638e8c1761de8e60473adde))

## [0.1.5](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.1.4...v0.1.5) (2023-08-16)


### Bug Fixes

* Исправлено получение доменов по mainDomain. Исправлено создание домена с поддоменами. Добавлен функционал добавления и удаления поддоменов. ([b97d739](https://gitlab.com/s3app-team/s3app/s3app-server/commit/b97d73946b3eba0f4da19e98d4528ab212893f6c))

## [0.1.4](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.1.3...v0.1.4) (2023-08-08)


### Bug Fixes

* remove stage desk ([69cf9a2](https://gitlab.com/s3app-team/s3app/s3app-server/commit/69cf9a2f822aeccca3b45a14fd0012f18c726b2a)), closes [#31](https://gitlab.com/s3app-team/s3app/s3app-server/issues/31)

## [0.1.3](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.1.2...v0.1.3) (2023-08-08)


### Bug Fixes

* A little refactoring of the subdomains. Subdomains are now added to the main domain. ([9548451](https://gitlab.com/s3app-team/s3app/s3app-server/commit/9548451d2e3689ee9e69ab420a779fe7bbb5f130)), closes [#30](https://gitlab.com/s3app-team/s3app/s3app-server/issues/30)

## [0.1.2](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.1.1...v0.1.2) (2023-08-06)


### Bug Fixes

* Исправление именования ([2291852](https://gitlab.com/s3app-team/s3app/s3app-server/commit/229185228043024d87be66ee35dc16fb987955d1))

## [0.1.1](https://gitlab.com/s3app-team/s3app/s3app-server/compare/v0.1.0...v0.1.1) (2023-07-30)


### Bug Fixes

* Восстановление пароля и смена пароля ([ee8714c](https://gitlab.com/s3app-team/s3app/s3app-server/commit/ee8714c27302ac9858ddbfce7a97fc44a1e1025e))
