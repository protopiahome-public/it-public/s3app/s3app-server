ALTER TABLE public."Comment"
    ADD COLUMN "tensionId" uuid,
    ADD COLUMN "driverId" uuid,
    ADD COLUMN "proposalId" uuid;