CREATE TABLE IF NOT EXISTS public."IssueTypeStage"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "issueTypeId" uuid NOT NULL,
    "stageId" uuid NOT NULL,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    CONSTRAINT "IssueTypeStage_pkey" PRIMARY KEY ("issueTypeId", "stageId"),
    CONSTRAINT fk_issuetypeid FOREIGN KEY ("issueTypeId")
        REFERENCES public."IssueType" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_stageid FOREIGN KEY ("stageId")
        REFERENCES public."Stage" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

ALTER TABLE IF EXISTS public."Stage"
    ADD COLUMN "isStart" boolean DEFAULT false;

ALTER TABLE IF EXISTS public."Stage"
    ADD COLUMN "isEnd" boolean DEFAULT false;
