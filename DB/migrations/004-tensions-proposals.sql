ALTER TABLE IF EXISTS public."Issue"
    ADD COLUMN "isClosed" boolean NOT NULL DEFAULT false;

ALTER TABLE IF EXISTS public."Issue"
    ADD COLUMN "closedAt" timestamp without time zone;

ALTER TABLE IF EXISTS public."Issue"
    ADD COLUMN "deadlineAt" timestamp without time zone;

CREATE TYPE public.driver_effect AS ENUM
    ('+', '-', '0');

ALTER TABLE IF EXISTS public."Driver"
    ADD COLUMN "driverEffect" driver_effect;

ALTER TABLE IF EXISTS public."Tension"
    ALTER COLUMN "tensionMark" DROP NOT NULL;

ALTER TABLE IF EXISTS public."Proposal"
    ADD COLUMN "previousProposalId" uuid;

ALTER TABLE IF EXISTS public."Proposal"
    ADD FOREIGN KEY ("previousProposalId")
    REFERENCES public."Proposal" (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

ALTER TABLE IF EXISTS public."Stage"
    ADD COLUMN "order" integer NOT NULL DEFAULT 0;

ALTER TABLE IF EXISTS public."Stage"
    ADD COLUMN color character varying;

CREATE TABLE IF NOT EXISTS public."DriverIssue"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "driverId" uuid NOT NULL,
    "issueId" uuid NOT NULL,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    CONSTRAINT "DriverIssue_pkey" PRIMARY KEY ("driverId", "issueId"),
    CONSTRAINT fk_driverid FOREIGN KEY ("driverId")
        REFERENCES public."Driver" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_issueid FOREIGN KEY ("issueId")
        REFERENCES public."Issue" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
