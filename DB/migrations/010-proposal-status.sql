ALTER TYPE public.proposal_status
    ADD VALUE 'ceased' AFTER 'rejected';
ALTER TYPE public.proposal_status
    ADD VALUE 'review' AFTER 'ceased';