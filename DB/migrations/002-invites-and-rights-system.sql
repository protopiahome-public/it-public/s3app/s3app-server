ALTER TABLE public."Proposal"
    ADD COLUMN "domainId" uuid NOT NULL,
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId")
        REFERENCES public."Domain" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID;

ALTER TABLE public."Tension"
    ADD COLUMN "domainId" uuid NOT NULL,
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId")
        REFERENCES public."Domain" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID;

CREATE TYPE public.organization_access_role_type AS ENUM
    ('user', 'admin');

CREATE TABLE public."UserOrganizationLink"
(
    "userId" uuid NOT NULL,
    "organizationId" uuid NOT NULL,
    "accessRole" organization_access_role_type NOT NULL,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    CONSTRAINT "UserOrganizationLink_pkey" PRIMARY KEY ("userId", "organizationId"),
    CONSTRAINT "UserOrganizationLink_organizationid_fkey" FOREIGN KEY ("organizationId")
        REFERENCES public."Domain" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT "UserOrganizationLink_userId_fkey" FOREIGN KEY ("userId")
        REFERENCES public."User" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

INSERT INTO "UserOrganizationLink" ("userId", "organizationId", "accessRole")
SELECT "User".id as user_id, "Domain".id as domain_id, 'admin'
FROM "User", "Domain"
WHERE "Domain"."domainType" = 'organization'
ORDER BY user_id;

CREATE TABLE public."UserOrganizationInvite"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    email character varying(255) COLLATE pg_catalog."default" NOT NULL,
    name character varying(255) COLLATE pg_catalog."default",
    license character varying(255) COLLATE pg_catalog."default",
    "organizationId" uuid NOT NULL,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "inviterUserId" uuid NOT NULL,
    "invitedUserId" uuid,
    "isAccepted" boolean NOT NULL DEFAULT false,
    "acceptedAt" timestamp without time zone,
    CONSTRAINT "UserOrganizationInvite_pkey" PRIMARY KEY (id),
    CONSTRAINT "UserOrganizationInvite_invitedUserId_fkey" FOREIGN KEY ("invitedUserId")
        REFERENCES public."User" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT "UserOrganizationInvite_inviterId_fkey" FOREIGN KEY ("inviterUserId")
        REFERENCES public."User" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT fk_organizationid FOREIGN KEY ("organizationId")
        REFERENCES public."Domain" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

ALTER TABLE "Proposal"
ADD COLUMN "domainId" uuid NOT NULL;

ALTER TABLE "Proposal"
ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId")
        REFERENCES public."Domain" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID;

ALTER TABLE "Tension"
ADD COLUMN "domainId" uuid NOT NULL;

ALTER TABLE "Tension"
ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId")
        REFERENCES public."Domain" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID;