FROM node:18
RUN mkdir app
WORKDIR /app
COPY package.json package.json
RUN npm install yarn typescript && yarn install
COPY . .
RUN cp src/libs/config.docker.sample.ts src/libs/config.ts
RUN yarn build
RUN mkdir -p dist/API/schema
RUN cp src/API/schema/api.graphql dist/API/schema/api.graphql
ENV S3APP_PORT=80
ENV S3APP_MONGO_URI=${S3APP_MONGO_URI}
ENV S3APP_JWT_SECRET_KEY=${S3APP_JWT_SECRET_KEY}
CMD ["npm", "run", "prod"]
