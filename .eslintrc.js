module.exports = {
  root: true,
  extends: 'airbnb-typescript/base',
  plugins: ['import', 'prettier'],
  'overrides': [
    {
      'files': ['./**/*.ts'],
      'parserOptions': {
        'project': './tsconfig.json',
      },
    },
  ],
  ignorePatterns: ['node_modules', 'dist', '.eslintrc.js'],
};