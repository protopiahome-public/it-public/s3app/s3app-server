import { ISprint } from '../../types/ISprint';
import { IUser } from '../../types/IUser';
import dbClient from '../../utils/knex';
import { NotFoundError } from '../erros';
import BaseRepository from './baseRepository';
import { EventType, IEvent } from '../../types/IEvent';
import { DomainRepositories } from './domainRepositories';
import { IssueRepositories } from './issueRepositories';
import { EventRepositories } from './eventRepositories';
import { IIssue } from '../../types/IIssue';
import { UserRepositories } from './userRepositories';
import { IDomain } from '../../types/IDomain';
export class SprintRepositories extends BaseRepository<ISprint> {

  getTable(): string {
    return 'Sprint';
  }

  public async getByDomain(domainId: string, withSubDomains: boolean = false) {
    if (withSubDomains) {
      const subdomains = (await this.domainHierarchy)?.findSubDomains(domainId);
      console.log(subdomains);
      return this.getByQuery(_knex => 
        _knex.whereIn(
          'domainId',
          subdomains?.map((domain) => domain.id) || [],
        ));
    }
    return this.getByFields({ domainId });
  }

  private async returnDomainUserIds(domainId: string) {
    const domainRepositories = new DomainRepositories(this.contextUser, domainId);
    const domain = domainRepositories.getData();
    if (!domain) {
      throw new NotFoundError('Domain not found');
    }
    const participantIds = await domainRepositories.getParticipantIds();

    return participantIds;
  }

  public async checkEntityAccess(entity: ISprint): Promise<boolean> {
    if (this.domainHierarchy && entity.domainId) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      if (!domainIds.includes(entity.domainId as string)) {
        throw new Error('Access denied');
      }
    }
    return !!entity;  
  }

  private async updateIssuesSprint(issueIds: string[]) {
    const data = await this.data;
    await new IssueRepositories(this.contextUser).checkEntitiesOfOrganization(issueIds, data?.domainId as string);
    for (const issue of issueIds) {
      const issueUpdate = {
        sprintId: this.id,
      };
      await new IssueRepositories(this.contextUser, issue).edit(issueUpdate);
    }
  }

  public async create(data: Partial<ISprint>): Promise<ISprint> {
    // if (!data.endDateForPlanningEvent || !data.endDateForReviewEvent) {
    //     throw new ValidationError('End date for planning and review events are required')
    // }
    const domain = await new DomainRepositories(this.contextUser, data.domainId).getData();
    if (!domain) {
      throw new NotFoundError('Domain not found');
    }
    let { participants } = data;
    delete data.participants;

    if (data.domainId && (!participants || !participants.length)) {
      const participantIds = await this.returnDomainUserIds(data.domainId);
      if (participantIds && participantIds.length) {
        data.participants = participantIds;
      }
    }
       
    if (data.endDateForPlanningEvent) {
      const planningEvent: Partial<IEvent> = {
        name: 'Planning Event',
        type: EventType.planning,
        startDate: data.startDate,
        endDate: data.endDateForPlanningEvent,
        participants: data.participants,
        domainId: data.domainId,
      };
      delete data.endDateForPlanningEvent;
      data.planningEventId = (await new EventRepositories(this.contextUser).create(planningEvent)).id;
    }
    if (data.endDateForReviewEvent) {
      const reviewEvent: Partial<IEvent> = {
        name: 'Review Event',
        type: EventType.review,
        startDate: data.endDate,
        endDate: data.endDateForReviewEvent,
        participants: data.participants,
        domainId: data.domainId,
      };
      delete data.endDateForReviewEvent;
      data.reviewEventId = (await new EventRepositories(this.contextUser).create(reviewEvent)).id;
    }
    const { issues } = data;
    delete data.issues;
    const sprint = await super.create(data);
    if (participants?.length && sprint.id) {
      this.addUsers(participants);
    }
    if (issues && sprint.id) {
      await this.updateIssuesSprint(issues);
    }

    return sprint;
  }

  public async changeIssuesForSpint(issueIds: string[]) {
    const data = await this.data;
    const issueRepositories = new IssueRepositories(this.contextUser);
    await issueRepositories.checkEntitiesOfOrganization(issueIds, data?.domainId as string);
    const allIssuesExist = await new IssueRepositories(this.contextUser).checkIssuesExist(issueIds);
    if (!allIssuesExist) {
      throw new NotFoundError('Some issues do not exist');
    }
    for (const issueId of issueIds) {
      const issueUpdate = {
        sprintId: this.id,
      };
      await new IssueRepositories(this.contextUser, issueId).edit(issueUpdate);
    }
    const sprintIssues = await new IssueRepositories(this.contextUser).getByFields({ 'sprintId': this.id });
    const issuesToDelete = sprintIssues.filter(x => !issueIds.includes(x.id));

    for (let i in issuesToDelete) {
      const issueUpdate = {
        sprintId: null,
      };
      await new IssueRepositories(this.contextUser, issuesToDelete[i].id).edit(issueUpdate);
    }
        
    return true;
  }

  public async edit(updateSprint: ISprint): Promise<ISprint> {
    if (updateSprint.domainId) {
      const domain = await new DomainRepositories(this.contextUser, updateSprint.domainId).getData();
      if (!domain) {
        throw new NotFoundError('Domain not found');
      }
    }
    const participantsData = (await this.getParticipants() || []).map(x => x.id);
    updateSprint.updatedAt = new Date();
    const { participants, issues } = updateSprint;
    delete updateSprint.participants;
    delete updateSprint.issues;
    const sprint = await super.edit(updateSprint);
    if (participants && participantsData) {
      const participantsToAdd = participants.filter(x => !participantsData.includes(x));
      const participantsToRemove = participantsData.filter(x => !participants.includes(x));
      if (participantsToAdd.length) {
        await this.addUsers(participantsToAdd);
      }
      if (participantsToRemove.length) {
        await this.removeUsers(participantsToRemove || []);
      }
    }
    if (issues) {
      await this.changeIssuesForSpint(issues);
    }
    return sprint;
  }

  async checkCreateEditData(newData: Partial<ISprint>, isCreate: boolean = false) {
    const data = isCreate ? undefined : await this.data;
    const domainId = (isCreate ? newData.domainId : data?.domainId) as string;
    if (newData.issues) {
      const issueRepositories = new IssueRepositories(this.contextUser);
      await issueRepositories.checkEntitiesOfOrganization(newData.issues, domainId);
    }
    if (newData.participants) {
      const userRepositories = new UserRepositories(this.contextUser);
      await userRepositories.checkEntitiesOfOrganization(newData.participants, domainId);
    }
    if (newData.planningEventId) {
      const eventRepositories = new EventRepositories(this.contextUser, newData.planningEventId);
      await eventRepositories.checkEntitiesOfOrganization([newData.planningEventId], domainId);
    }
    if (newData.reviewEventId) {
      const eventRepositories = new EventRepositories(this.contextUser, newData.reviewEventId);
      await eventRepositories.checkEntitiesOfOrganization([newData.reviewEventId], domainId);
    }
    return !!newData;
  }

  public async checkEntitiesOfOrganization(ids: string[], domainId: string): Promise<ISprint[]> {
    const entities = await this.getByIds(ids);
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const organizationId = domainHierarchy.getOrganizationFromDomain(domainId)?.domain.id;
      if (!organizationId) {
        throw new Error('Access denied');
      }
      entities.forEach(entity => {
        if (!domainHierarchy.checkDomainInOrganization(entity.domainId as string, organizationId)) {
          throw new Error('Access denied');
        }
      });
    }

    return entities;
  }

  public async getByUser(userId: string): Promise<ISprint[]> {
    const sprintIds = await dbClient('UserSprint')
      .where('userId', userId)
      .orderBy('createdAt', 'desc')
      .pluck('sprintId');
    return this.getByIds(sprintIds);
  }

  public async addUsers(participants: string[]): Promise<boolean | never> {
    await this.getData();
    const insertData = participants.map(userId => ({
      sprintId: this.id,
      userId: userId,
    }));
    await dbClient('UserSprint')
      .insert(insertData)
      .returning('id');

    return true;
  }

  public async getParticipants(): Promise<IUser[]> {
    const users = await dbClient('User')
      .leftJoin('UserSprint', 'User.id', 'UserSprint.userId')
      .select('User.*')
      .where('UserSprint.sprintId', this.id)
      .andWhere('UserSprint.isDeleted', false)
      .orderBy('UserSprint.createdAt', 'desc');
    return users;
  }

  public async removeUsers(userIds: string[]): Promise<boolean | never> {
    await this.getData();
    await dbClient('UserSprint')
      .where('sprintId', this.id)
      .whereIn('userId', userIds)
      .update({
        isDeleted: true,
        deletedAt: new Date(),
      });
    return true;
  }

  public async getPlanningEvent(): Promise<IEvent | null> {
    const data = await this.data;
    if (!data?.planningEventId) {
      return null;
    }
    return new EventRepositories(this.contextUser, data.planningEventId).getData();
  }

  public async getReviewEvent(): Promise<IEvent | null> {
    const data = await this.data;
    if (!data?.reviewEventId) {
      return null;
    }
    return new EventRepositories(this.contextUser, data.reviewEventId).getData();
  }

  public async getIssues(): Promise<IIssue[]> {
    return new IssueRepositories(this.contextUser).getBySprint(this.id as string);
  }
  
  public async getDomain(): Promise<IDomain> {
    const data = await this.data;
    return new DomainRepositories(this.contextUser, data?.domainId).getData();
  }
}