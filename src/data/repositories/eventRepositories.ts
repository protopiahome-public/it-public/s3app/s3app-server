import { IEvent } from '../../types/IEvent';
import dbClient from '../../utils/knex';
import { NotFoundError } from '../erros';
import dayjs from 'dayjs';
import BaseRepository from './baseRepository';
import { IUser } from '../../types/IUser';
import { transporter } from '../../utils/email';
import { UserRepositories } from './userRepositories';
import { DomainRepositories } from './domainRepositories';
import { IDomain } from '../../types/IDomain';
import config from '../../libs/config';

export class EventRepositories extends BaseRepository<IEvent> {
  getTable(): string {
    return 'Event';
  }

  public async getByDomain(domainId: string, withSubDomains: boolean) {
    if (withSubDomains) {
      const subdomains = (await this.domainHierarchy)?.findSubDomains(
        domainId,
      );
      return this.getByQuery(_knex => 
        _knex.whereIn(
          'domainId',
          subdomains?.map((domain) => domain.id) || [],
        ));
    }
    return this.getByFields({ domainId });
  }

  public async create(data: Partial<IEvent>) {
    const participants = data.participants;
    delete data.participants;
    let newEvent = await super.create(data);
    if (participants && participants.length) {
      await this.sendEmailsToParticipants(participants);
      await dbClient('EventParticipant').insert((participants || []).map((participant) => ({ eventId: newEvent.id, userId: participant })));
    }
    return newEvent;
  }

  public async edit(newData: Partial<IEvent>): Promise<IEvent> {
    const participants = newData.participants;
    delete newData.participants;
    if (participants) {
      await this.changeParticipants(participants);
    }
    return super.edit(newData);
  }

  public async getParticipants() {
    const participantIds = await this.getParticipantIds();
    let participants: IUser[] = [];
    if (participantIds?.length) {
      participants = await new UserRepositories(this.contextUser).getByIds(participantIds);
    }
    return participants;
  }

  public async checkEntityAccess(entity: IEvent): Promise<boolean> {
    if (this.domainHierarchy && entity.domainId) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      if (!domainIds.includes(entity.domainId as string)) {
        throw new Error('Access denied');
      }
    }
    return !!entity;  
  }

  public async checkCreateEditData(newData: Partial<IEvent>, isCreate?: boolean): Promise<boolean> {
    newData.startDate = new Date(newData.startDate || '');
    newData.endDate = new Date(newData.endDate || '');
    const data = isCreate ? undefined : await this.data;
    const domainId = isCreate ? newData.domainId : data?.domainId;
    if (newData.facilitatorId) {
      const userRepositories = new UserRepositories(this.contextUser);
      await userRepositories.checkEntitiesOfOrganization([newData.facilitatorId], domainId as string);
    }
    if (newData.participants) {
      const userRepositories = new UserRepositories(this.contextUser);
      await userRepositories.checkEntitiesOfOrganization(newData.participants, domainId as string);
    }

    return true;
  }

  public async checkEntitiesOfOrganization(ids: string[], domainId: string): Promise<IEvent[]> {
    const entities = await this.getByIds(ids);
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const organizationId = domainHierarchy.getOrganizationFromDomain(domainId)?.domain.id;
      if (!organizationId) {
        throw new Error('Access denied');
      }
      entities.forEach(entity => {
        if (!domainHierarchy.checkDomainInOrganization(entity.domainId as string, organizationId)) {
          throw new Error('Access denied');
        }
      });
    }

    return entities;
  }

  public async addParticipants(userIds: string[]) {
    const data = await this.data;
    const allUserExist = await new UserRepositories(this.contextUser).checkUsersExist(userIds);
    if (!allUserExist) {
      throw new NotFoundError('Some users do not exist');
    }
    const userRepositories = new UserRepositories(this.contextUser);
    await userRepositories.checkEntitiesOfOrganization(userIds, data?.domainId as string);
    const inserDate = userIds.map(userId => ({
      eventId: this.id,
      userId: userId,
    }));
    if (inserDate.length) {
      await dbClient('EventParticipant')
        .insert(inserDate)
        .returning('id');
    }
    return true;
  }

  public async removeParticipants(userIds: string[]) {
    await this.getData();            
    const allUserExist = await new UserRepositories(this.contextUser).checkUsersExist(userIds);
    if (!allUserExist) {
      throw new NotFoundError('Some users do not exist');
    }
    await dbClient('EventParticipant')
      .where('eventId', this.id)
      .whereIn('userId', userIds)
      .update({
        isDeleted: true,
        deletedAt: dayjs().format('YYYY-MM-DD HH:mm:ss'),
      });
    return true;
  }
  // public async getByFieldAndId(
  //     field: string,
  //     id: string
  // ): Promise<void | object | undefined> {
  //     return await this.GetByFieldAndId("participants", id);
  // }
  
  public async changeParticipants(userIds: string[]): Promise<boolean> {
    await this.getData();
    const currentParticipantIds = await this.getParticipantIds();
    const toAdd = userIds.filter((userId) => !currentParticipantIds.includes(userId));
    const toDelete = currentParticipantIds.filter((userId) => !userIds.includes(userId));
    await this.addParticipants(toAdd);
    await this.removeParticipants(toDelete);
    return true;
  }

  public async sendEmailsToParticipants(participants: string[]) {
    return;
    await this.getData();
    let emails = await new UserRepositories(this.contextUser).getEmailsByIdList(participants);
    if (!emails) {
      throw new Error('No emails');
    }
    let result = await transporter.sendMail({
      from: `S3APP <${config.emailTransporter?.auth.user}>`,
      to: emails,
      subject: 'Уведомление о встречи',
      text: 'Встреча назначена',
    });
    console.log(result);
  }

  public async getFacilitatorEvents(userId: string): Promise<IEvent[]> {
    const events = await dbClient('Event')
      .where('facilitatorId', userId)
      .andWhere('isDeleted', false)
      .select('*')
      .orderBy('createdAt', 'desc');
    return events;
  }

  public async getParticipantIds(): Promise<string[]> {
    return dbClient('EventParticipant')
      .where('eventId', this.id)
      .where('isDeleted', false)
      .pluck('userId')
      .orderBy('createdAt', 'desc');
  }

  public async getDomain(): Promise<IDomain | null> {
    const data = await this.data;
    if (!data?.domainId) {
      return null;
    }
    return new DomainRepositories(this.contextUser, data?.domainId).getData();
  }

  public async getFacilitator(): Promise<IUser | null> {
    const data = await this.data;
    if (!data?.domainId) {
      return null;
    }
    return new UserRepositories(this.contextUser, data?.facilitatorId).getData();
  }
}