import { IUser } from '../../types/IUser';
import { NotFoundError } from '../erros';
import bcrypt from 'bcrypt';
import dbClient from '../../utils/knex';
import { IUserOrganizationLink } from '../../types/IUserOrganizationLink';
import BaseRepository from './baseRepository';
import { AuthenticationError, ForbiddenError, UserInputError } from 'apollo-server-express';
import { EventRepositories } from './eventRepositories';
import { DomainRepositories } from './domainRepositories';
import { IIssue } from '../../types/IIssue';
import { IssueRepositories } from './issueRepositories';
import { ISprint } from '../../types/ISprint';
import { SprintRepositories } from './sprintRepositories';
import { IComment } from '../../types/IComment';
import { CommentRepositories } from './commentRepositories';
import { IUserResDTO } from '../../types/DTO/IUserResDTO';
import { Knex } from 'knex';
export class UserRepositories extends BaseRepository<IUser> {
  getTable(): string {
    return 'User';
  }

  needContextUser(): boolean | undefined {
    return undefined;
  }

  public async getPartiticpantEvents() {
    const eventIds = await this.getParticipantEventIds();
    const events = await new EventRepositories(this.contextUser).getByIds(eventIds);
    return events;
  }

  public async getFacilitatorEvents() {
    return new EventRepositories(this.contextUser).getFacilitatorEvents(this.id as string);
  }

  public async getUserDomains() {
    const domainIds = await this.getUserDomainIds();
    const domains = await new DomainRepositories(this.contextUser).getByIds(domainIds);
    return domains;
  }

  public async create(newData: Partial<IUser>) {
    if (!newData.password) throw new UserInputError('The transferred user does not have a password');
    // user.email = user.email.trim().toLowerCase();
    let model = await this.getByEmail(newData.confirmEmail as string);
    if (model) {
      throw new AuthenticationError(
        'A user with this email or username already exists',
      );
    }

    const entity = await dbClient('User')
      .where('email', newData.confirmEmail)
      .first();
    if (entity) throw new NotFoundError('A user with this e-mail address is already in the database');
    return super.create(newData);
  }

  async filterEntities(knex: Knex.QueryBuilder) {
    if (this.domainHierarchy) {
      const userOgranizationIds = await (await this.domainHierarchy).getUserOrganizationsIds(this.contextUser?.id as string);

      return knex.leftJoin('UserOrganizationLink', 'UserOrganizationLink.userId', 'User.id')
        .whereIn('UserOrganizationLink.organizationId', userOgranizationIds).distinct();
    }
    return knex;
  }

  public async checkEntitiesOfOrganization(ids: string[], domainId: string): Promise<IUser[]> {
    const entities = await this.getByIds(ids);
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const organizationId = domainHierarchy.getOrganizationFromDomain(domainId)?.domain.id;
      if (!organizationId) {
        throw new Error('Access denied');
      }
      for (let i in entities) {
        const entity = entities[i];
        if (this.contextUser && entity.id === this.contextUser?.id) {
          continue;
        }
        if (!(await domainHierarchy.getUserOrganizationsIds(entity.id as string)).includes(organizationId)) {
          throw new Error('Access denied');
        }
      }
    }

    return entities;
  }

  async checkEntityAccess(entity: IUser): Promise<boolean> {
    if (this.contextUser?.id && this.id === this.contextUser.id) {
      return !!entity;
    }
    if (this.domainHierarchy) {
      const contextUserOgranizationIds = await (await this.domainHierarchy).getUserOrganizationsIds(this.contextUser?.id as string);
      const entityUserOgranizationIds = await (await this.domainHierarchy).getUserOrganizationsIds(this.id as string);
      if (!contextUserOgranizationIds.some((id) => entityUserOgranizationIds.includes(id))) {
        throw new Error('Access denied');
      }
    }
    return !!entity;
  }

  public async checkUsersExist(userIds: string[]): Promise<boolean> {
    const users = await this.getByIds(userIds);
    return (users || []).length === userIds.length;
  }

  public async getByEmail(email: string): Promise<IUser | null> {
    return (await this.getByFields({ email }))[0];
  }

  public async getByName(name: string): Promise<IUser | null> {
    return (await this.getByFields({ name }))[0];
  }

  public async getEmailsByIdList(participants: string[]): Promise<string[]> {
    const emails = await dbClient('User')
      .where('id', 'in', participants)
      .where('isDeleted', false)
      .orderBy('createdAt', 'desc')
      .pluck('email');
    return emails;
  }

  public async getParticipantEventIds(): Promise<string[]> {
    const eventIds = await dbClient('EventParticipant')
      .where('userId', this.id)
      .orderBy('createdAt', 'desc')
      .pluck('eventId');
    return eventIds;
  }

  public async getUserDomainIds(): Promise<string[]> {
    const domainIds = await dbClient('UserDomainLink')
      .where('userId', this.id)
      .orderBy('createdAt', 'desc')
      .select('domainId')
      .pluck('domainId');
    return domainIds;
  }

  public async getUserOrganizations(): Promise<IUserOrganizationLink[]> {
    return dbClient('UserOrganizationLink')
      .where('userId', this.id)
      .orderBy('createdAt', 'desc');
  }

  public async getExecutorInIssues(): Promise<IIssue[]> {
    return new IssueRepositories(this.contextUser).getByExecutorUser(this.id as string);
  }

  public async getAuthorInIssues(): Promise<IIssue[]> {
    return new IssueRepositories(this.contextUser).getByAuthorUser(this.id as string);
  }

  public async getSprints(): Promise<ISprint[]> {
    return new SprintRepositories(this.contextUser).getByUser(this.id as string);
  }

  public async getComments(): Promise<IComment[]> {
    return new CommentRepositories(this.contextUser).getByFields({
      authorUserId: this.id,
    });
  }

  public async changePassword(
    oldPassword: string,
    newPassword: string,
  ) {
    const user = await this.data;
    if (!user?.password) throw new ForbiddenError('The user in the database does not have a password');
    const isPasswordCorrect = await bcrypt.compare(
      oldPassword,
      user.password,
    );
    if (!isPasswordCorrect) {
      throw new AuthenticationError('The old password is incorrect');
    }

    const newPasswordHash = await bcrypt.hash(newPassword, 10);
    this.edit({ password: newPasswordHash });
    const userDTO: IUserResDTO = {
      id: user.id,
      name: user.name,
      email: user.email,
    };
    return userDTO;
  }

  public async getOfOrganization(domainId: string): Promise<IUser[]> {
    const organization = (await this.domainHierarchy)?.getOrganizationFromDomain(domainId);
    return this.getByQuery((query) => 
      query
      // .leftJoin('UserOrganizationLink', 'UserOrganizationLink.userId', 'User.id')
        .where('UserOrganizationLink.organizationId', organization?.domain.id)
        .distinct());
  }    
}
