import { UserRepositories } from './userRepositories';
import { GraphQLClient } from 'graphql-request';
import config from '../../libs/config';
import { AuthenticationError } from 'apollo-server-express';

const client = new GraphQLClient(config.licenseRepository, {
  headers: {
    authorization: `Bearer ${config.licenseRepositoryAccessToken}`,
  },
});

export type ILicense = {
  id: string;
  purchaseId: string;
  licenseKey: string;
  expireAt: Date;
  createdAt: Date;
  activated: boolean;
  activatedAt: Date;
  productId: string;
  userOwnerId: string;
  name: string;
};

export class LicenseRepositories extends UserRepositories {
  needContextUser(): boolean | undefined {
    return undefined;
  }

  public async updateLicense(licenseKey: string): Promise<boolean> {
    if (!this.contextUser) {
      throw new AuthenticationError('Access denied');
    }
    let licenseObject: ILicense;
    try {
      licenseObject = await this.getLicense(licenseKey);
    } catch (error) {
      throw new AuthenticationError('Лицензия не найдена');
    }
    if (licenseObject.activated) {
      throw new AuthenticationError('Лицензия уже активирована');
    }
    if (licenseObject.expireAt < new Date()) {
      throw new AuthenticationError('Лицензия истекла');
    }
    await this.activateLicense(licenseKey);
    await this.edit({
      license: licenseKey,
      licenseExpires: licenseObject.expireAt,
    });
    return true;
  }

  public async updateLicenses(): Promise<void> {
    const users = await this.getAll() || [];
    users.forEach(async (user) => {
      if (user.license && user.licenseExpires) {
        try {
          const licenseObject = await this.getLicense(user.license);
          await new LicenseRepositories(this.contextUser, user.id).edit({
            licenseExpires: licenseObject.expireAt,
          });
        } catch (error) {
          console.error(error);
        }
      }
    });
  }

  public async getLicense(licenseKey: string): Promise<ILicense> {
    return (await client.request<{ getLicenseKey: ILicense }>(`
        query($licenseKey: String!) {
            getLicenseKey(licenseKey: $licenseKey) {
              expireAt
              activated
            }
          }
        `, { licenseKey })).getLicenseKey as ILicense;
  }
    
  public async activateLicense(licenseKey: string): Promise<ILicense> {
    return (await client.request<{ activateLicenseKey: ILicense }>(`
        mutation($licenseKey: String!) {
            activateLicenseKey(licenseKey: $licenseKey) {
              activated
              expireAt
            }
          }
        `, { licenseKey })).activateLicenseKey as ILicense;
  }
}