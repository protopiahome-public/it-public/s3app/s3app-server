import { Knex } from 'knex';
import { IComment } from '../../types/IComment';
import { IUser } from '../../types/IUser';
import BaseRepository from './baseRepository';
import { UserRepositories } from './userRepositories';
import { IssueRepositories } from './issueRepositories';
import { TensionRepositories } from './tensionRepositories';
import { DriverRepositories } from './driverRepositories';
import { ProposalRepositories } from './proposalRepositories';
import { ThoughtRepositories } from './thoughtRepositories';

export class CommentRepositories extends BaseRepository<IComment> {
  contextUser?: IUser;
  
  public getTable() {
    return 'Comment';
  }

  public async getByDriver(driverId: string) {
    return this.getByFields({ driverId });
  }

  public async getByProposal(proposalId: string) {
    return this.getByFields({ proposalId });
  }

  public async getByIssue(issueId: string) {
    return this.getByFields({ issueId });
  }

  public async getByTension(tensionId: string) {
    return this.getByFields({ tensionId });
  }

  public async getByThought(thoughtId: string) {
    return this.getByFields({ thoughtId });
  }

  public filterEntities(knex: Knex.QueryBuilder<any, any>): Promise<any> {
    return knex;
  }

  public async checkCreateEditData(
    newData: Partial<IComment>,
    isCreate?: boolean,
  ): Promise<boolean> {
    if (isCreate) {
      let relations = 0;
      if (
        !('issueId' in newData) &&
        !('thoughtId' in newData) &&
        !('tensionId' in newData) &&
        !('driverId' in newData) &&
        !('proposalId' in newData)
      ) {
        throw new Error('Entity Id are required');
      }
      if ('issueId' in newData) {
        const issue = new IssueRepositories(
          this.contextUser,
          newData.issueId,
        ).getData();
        if (!issue) {
          throw new Error('Issue not found');
        }
        relations++;
      } else if ('thoughtId' in newData) {
        const tension = new ThoughtRepositories(
          this.contextUser,
          newData.thoughtId,
        ).getData();
        if (!tension) {
          throw new Error('Thought not found');
        }
        relations++;
      } else if ('tensionId' in newData) {
        const tension = new TensionRepositories(
          this.contextUser,
          newData.tensionId,
        ).getData();
        if (!tension) {
          throw new Error('Tension not found');
        }
        relations++;
      } else if ('driverId' in newData) {
        const driver = new DriverRepositories(
          this.contextUser,
          newData.driverId,
        ).getData();
        if (!driver) {
          throw new Error('Driver not found');
        }
        relations++;
      } else if ('proposalId' in newData) {
        const proposal = new ProposalRepositories(
          this.contextUser,
          newData.proposalId,
        ).getData();
        relations++;
        if (!proposal) {
          throw new Error('Proposal not found');
        }
      } 
      if (relations !== 1) {
        throw new Error('More than one relation');
      }
    }

    return true;
  }

  public async create(newData: Partial<IComment>): Promise<IComment> {
    newData.authorUserId = this.contextUser?.id;
    if (
      !('issueId' in newData) && 
      !('thoughtId' in newData) && 
      !('tensionId' in newData) && 
      !('driverId' in newData) && 
      !('proposalId' in newData)
    ) {
      throw new Error('Entity Id are required');
    }
    return super.create(newData);
  }

  public async edit(newData: Partial<IComment>): Promise<IComment> {
    const data = await this.data;
    if (data?.authorUserId !== this.contextUser?.id) {
      throw new Error('Access denied');
    }
    return super.edit(newData);
  }

  public async getAuthor(): Promise<IUser> {
    const data = await this.data;
    return new UserRepositories(
      this.contextUser,
      data?.authorUserId,
    ).getData();
  }
}
