import { IStage } from '../../types/IStage';
import BaseRepository from './baseRepository';
import { IDomain } from '../../types/IDomain';
import { DomainRepositories } from './domainRepositories';
import { IIssue } from '../../types/IIssue';
import { IssueRepositories } from './issueRepositories';
import dbClient from '../../utils/knex';

export class StageRepositories extends BaseRepository<IStage> {

  getTable(): string {
    return 'Stage';
  }

  public async getByDomainId(domainId: string) {
    return this.getByFields({ domainId });
  }

  public async getDomain(): Promise<IDomain> {
    const data = await this.data;
    return new DomainRepositories(this.contextUser, data?.domainId).getData();
  }

  orderBy(): string {
    return `${this.getTable()}.order`;
  }

  orderByDirection(): string {
    return 'asc';
  }

  public async checkCreateEditData(newData: Partial<IStage>): Promise<boolean> {
    if (newData.isStart) {
      newData.isEnd = false;
    }

    if (newData.isEnd) {
      newData.isStart = false;
    }
    
    return true;
  }

  public async checkEntityAccess(entity: IStage): Promise<boolean> {
    if (this.domainHierarchy && entity.domainId) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      if (!domainIds.includes(entity.domainId as string)) {
        throw new Error('Access denied');
      }
    }
    return !!entity;  
  }

  public async checkEntitiesOfOrganization(ids: string[], domainId: string): Promise<IStage[]> {
    const entities = await this.getByIds(ids);
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const organizationId = domainHierarchy.getOrganizationFromDomain(domainId)?.domain.id;
      if (!organizationId) {
        throw new Error('Access denied');
      }
      entities.forEach(entity => {
        if (!domainHierarchy.checkDomainInOrganization(entity.domainId as string, organizationId)) {
          throw new Error('Access denied');
        }
      });
    }

    return entities;
  }

  public async getIssues(): Promise<IIssue[]> {
    const data = await this.data;
    return new IssueRepositories(this.contextUser).getByStage(data?.id as string);
  }

  public async order(stageIds: string[]): Promise<IStage[]> {
    for (let k in stageIds) {
      const id = stageIds[k];
      await dbClient(this.getTable()).where('id', id).update('order', k);
    }
    return this.getByIds(stageIds);
  }
}