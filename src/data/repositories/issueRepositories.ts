import { IIssue } from '../../types/IIssue';
import dbClient from '../../utils/knex';
import { IEntityId } from '../../types/IEntityId';
import BaseRepository from './baseRepository';
import { StageRepositories } from './stageRepositories';
import { IStage } from '../../types/IStage';
import { IUser } from '../../types/IUser';
import { UserRepositories } from './userRepositories';
import { IDomain } from '../../types/IDomain';
import { DomainRepositories } from './domainRepositories';
import { SprintRepositories } from './sprintRepositories';
import { DriverRepositories } from './driverRepositories';
import { NotFoundError } from '../erros';
import { IDriver } from '../../types/IDriver';
import { CommentRepositories } from './commentRepositories';
import { IssueTypeRepositories } from './issueTypeRepositories';
import { IIssueType } from '../../types/IIssueType';
import { Knex } from 'knex';

export interface IssueFilter {
  name: string;
  priority?: number
  stageName?: string
  issueTypeName?: string
  executorId?: string
  startDate?: Date
  endDate?: Date
  open?: boolean
  linkIssueId?: string
  domainId?: string
}

export interface IssueSort {
  field: 'createdAt' | 'name' | 'priority' | 'stage' | 'issueType' | 'author'
  | 'executor' | 'domain' | 'links';
  direction: 'asc' | 'desc';
}

export class IssueRepositories extends BaseRepository<IIssue> {
  getTable() {
    return 'Issue';
  }

  public async getByAuthorUser(authorId: string) {
    return this.getByFields({ 'authorUserId': authorId });
  }

  public async getByExecutorUser(executorId: string) {
    return this.getByFields({ 'executorUserId': executorId });
  }

  public async checkEntityAccess(entity: IIssue): Promise<boolean> {
    if (this.domainHierarchy && entity.domainId) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      if (!domainIds.includes(entity.domainId as string)) {
        throw new Error('Access denied');
      }
    }
    return !!entity;  
  }

  public async checkEntitiesOfOrganization(ids: string[], domainId: string): Promise<IIssue[]> {
    const entities = await this.getByIds(ids);
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const organizationId = domainHierarchy.getOrganizationFromDomain(domainId)?.domain.id;
      if (!organizationId) {
        throw new Error('Access denied');
      }
      for (let i in entities) {
        const entity = entities[i];
        if (!domainHierarchy.checkDomainInOrganization(entity.domainId as string, organizationId)) {
          throw new Error('Access denied');
        }
      }
    }

    return entities;
  }

  async filterEntities(knex: Knex.QueryBuilder) {
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      return knex.whereIn('Issue.domainId', domainIds);
    }
    return knex;
  }

  public async getByFilters(page: number, perPage: number, filters?: IssueFilter, sort?: IssueSort): Promise<IIssue[]> {
    return this.getByQuery((query) => this.getFilters(query, page, perPage, filters, sort));
  }

  public getFilters(query: Knex.QueryBuilder, page: number, perPage: number, filters?: IssueFilter, sort?: IssueSort):Knex.QueryBuilder {
    if (sort) {
      query.clearOrder();
    }
    query = query.groupBy('Issue.id');
    if (filters?.issueTypeName || sort?.field === 'issueType') {
      query = query.leftJoin('IssueType', 'Issue.issueTypeId', 'IssueType.id');
    }
    if (filters?.stageName || filters?.open || sort?.field === 'stage') {
      query = query.leftJoin('Stage', 'Issue.stageId', 'Stage.id');
    }
    if (filters?.linkIssueId || sort?.field === 'links') {
      query = query.leftJoin('IssueLink', 'Issue.id', 'IssueLink.fromIssueId');
    }
    if (sort?.field === 'author') {
      query = query.leftJoin('User as Author', 'Issue.authorUserId', 'Author.id');
    }
    if (sort?.field === 'executor') {
      query = query.leftJoin('User as Executor', 'Issue.executorUserId', 'Executor.id');
    }
    if (sort?.field === 'domain') {
      query = query.leftJoin('Domain', 'Issue.domainId', 'Domain.id');
    }
    if (sort?.field === 'links') {
      query = query.leftJoin('Issue as IssueLinkIssue', 'IssueLink.toIssueId', 'IssueLinkIssue.id');
    }

    if (filters?.name) {
      query = query.where(dbClient.raw('lower("Issue"."name")'), 'like', '%' + filters.name.toLowerCase() + '%');
    }
      
    if (filters?.priority) {
      query = query.where('Issue.priority', filters.priority);
    }
    if (filters?.stageName) {
      query = query.where('Stage.name', filters.stageName);
    }
    if (filters?.issueTypeName) {
      query = query.where('IssueType.name', filters.issueTypeName);
    }
    if (filters?.executorId) {
      query = query.where('Issue.executorUserId', filters.executorId);
    }
    if (filters?.startDate) {
      query = query.where('Issue.createdAt', '>=', filters.startDate);
    }
    if (filters?.endDate) {
      query = query.where('Issue.createdAt', '<=', filters.endDate);
    }
    if (filters?.open) {
      query = query.whereNot('Stage.isEnd', true);
    }
    if (filters?.linkIssueId) {
      query = query.where('IssueLink.toIssueId', filters.linkIssueId);
    }
    if (filters?.domainId) {
      query = query.where('Issue.domainId', filters.domainId);
    }

    if (sort?.field === 'author') {
      query = query.orderByRaw(`min("Author"."name") ${sort.direction}`);
    }
    if (sort?.field === 'executor') {
      query = query.orderByRaw(`min("Executor"."name") ${sort.direction}`);
    }
    if (sort?.field === 'domain') {
      query = query.orderByRaw(`min("Domain"."name") ${sort.direction}`);
    }
    if (sort?.field === 'links') {
      query = query.orderByRaw(`min("IssueLinkIssue"."name") ${sort.direction}`);
    }
    if (sort?.field === 'issueType') {
      query = query.orderByRaw(`min("IssueType"."name") ${sort.direction}`);
    }
    if (sort?.field === 'stage') {
      query = query.orderByRaw(`min("Stage"."name") ${sort.direction}`);
    }
    if (sort && [ 'createdAt', 'name', 'priority' ].includes(sort?.field)) {
      query = query.orderBy('Issue.' + sort.field, sort.direction);
    }

    if (perPage > 0) {
      query = query.limit(perPage).offset((page - 1) * perPage);
    }
    return query;
  }

  public async getByDomainQuery(domainId: string, withSubDomains: boolean = false, 
    page?: number, perPage?: number, filters?: IssueFilter, sort?: IssueSort): Promise<(_knex: Knex.QueryBuilder) => Knex.QueryBuilder> {
    let subdomains: IDomain[] | undefined;
    if (withSubDomains) {
      subdomains = (await this.domainHierarchy)?.findSubDomains(domainId);
    }
    return (_knex: Knex.QueryBuilder) => {
      if (withSubDomains) {
        _knex = _knex.whereIn(
          'Issue.domainId',
          subdomains?.map((domain) => domain.id) || [],
        );
      } else {
        _knex = _knex.where('Issue.domainId', domainId);
      }
      if (page && perPage) {
        _knex = this.getFilters(_knex, page, perPage, filters, sort);
      }
      return _knex;
    };
  }

  public async getByDomain(domainId: string, withSubDomains: boolean = false, 
    page?: number, perPage?: number, filters?: IssueFilter, sort?: IssueSort) {
    const query = await this.getByDomainQuery(domainId, withSubDomains, page, perPage, filters, sort);
    return this.getByQuery(query);
  }

  public async getByDomainCount(domainId: string, withSubDomains: boolean = false, 
    filters?: IssueFilter):Promise<number> {
    const query = await this.getByDomainQuery(domainId, withSubDomains, -1, -1, filters);
    return (await this.getByQuery(query)).length;
  }

  public async getByStage(stageId: string) {
    return this.getByFields({ 'stageId': stageId });
  }

  public async getBySprint(sprintId: string) {
    return this.getByFields({ 'sprintId': sprintId });
  }

  public async create(data: IIssue) {
    const { issueLinks } = data;
    delete data.issueLinks;
    const { driverIds } = data;
    delete data.driverIds;

    data.authorUserId = this.contextUser?.id;

    const issue = await super.create(data);

    if (issueLinks?.length && issue.id) {
      for (let toIssue of issueLinks) {
        await this.createIssueLink(toIssue);
      }
    }
    if (driverIds) {
      await this.linkIssueAndDrivers(driverIds);
    }
    return issue;
  }

  public async linkIssueAndDrivers(driversIds: string[]) {
    const drivers = await new DriverRepositories(this.contextUser).getByIds(driversIds);
    if (!drivers || drivers.length !== driversIds.length) {
      throw new NotFoundError('Drivers not found');
    }
    const issue = await this.getData();
    if (!issue) {
      throw new NotFoundError('Issue not found');
    }
    const insertData = driversIds.map(driverId => ({
      issueId: this.id,
      driverId,
    }));
    if (insertData.length) {
      const insertedDriverLinks = await dbClient('DriverIssue')
        .insert(insertData)
        .returning('id');
      return insertedDriverLinks;
    }
    return [];
  }

  public async unlinkIssueAndDrivers(driverIds: string[] | string[]): Promise<boolean> {
    await this.getData();
    if (driverIds.length) {
      await dbClient('DriverIssue')
        .delete()
        .where('issueId', this.id)
        .whereIn('driverId', driverIds);
      return true;
    }
    return true;
  }

  public async changeDriverLinks(driversIds: string[]): Promise<IEntityId> {
    const data = await this.data;
    await this.checkEntitiesOfOrganization(driversIds, data?.domainId as string);
    const currentDrivers = await this.getDrivers();
    const currentDriverLinksIds = currentDrivers.map((driver) => driver.id);
    const toAdd = driversIds.filter((driver) => !currentDriverLinksIds.includes(driver));
    const toDelete = currentDriverLinksIds.filter((driver) => !driversIds.includes(driver));
    await this.linkIssueAndDrivers(toAdd);
    await this.unlinkIssueAndDrivers(toDelete);
    return { id: this.id };
  }

  public async edit(updateIssue: Partial<IIssue>): Promise<IIssue> {
    const { issueLinks } = updateIssue;
    delete updateIssue.issueLinks;
    const { driverIds } = updateIssue;
    delete updateIssue.driverIds;
    
    delete updateIssue.authorUserId;

    if (issueLinks) {
      await this.changeIssueLinks(issueLinks);
    }
    if (driverIds) {
      await this.changeDriverLinks(driverIds);
    }
    return super.edit(updateIssue);
  }

  public async checkIssuesExist(ids: string[]): Promise<boolean> {
    const issues = await this.getByIds(ids);
    return issues.length == ids.length;
  }

  async checkCreateEditData(newData: Partial<IIssue>, isCreate: boolean = false) {
    const data = isCreate ? undefined : await this.data;
    const domainId = (isCreate ? newData.domainId : data?.domainId) as string;
    if (newData.executorUserId) {
      const userRepositories = new UserRepositories(this.contextUser);
      await userRepositories.checkEntitiesOfOrganization([newData.executorUserId], 
        domainId);
    }
    if (isCreate && !newData.stageId) {
      throw new Error('Stage is required');
    }
    if (newData.issueTypeId) {
      const issueTypeRepositories = new IssueTypeRepositories(this.contextUser, newData.issueTypeId);
      await issueTypeRepositories.checkEntitiesOfOrganization([newData.issueTypeId], 
        domainId);
      const stages = await issueTypeRepositories.getStages();
      if (newData.stageId && !stages.find(stage => stage.id === newData.stageId)) {
        throw new Error('Stage not found');
      }
    }
    if (newData.stageId) {
      const stageRepositories = new StageRepositories(this.contextUser);
      await stageRepositories.checkEntitiesOfOrganization([newData.stageId], 
        domainId);
    }
    if (newData.sprintId) {
      const sprintRepositories = new SprintRepositories(this.contextUser);
      await sprintRepositories.checkEntitiesOfOrganization([newData.sprintId], 
        domainId);
    }
    if (newData.issueLinks) {
      await this.checkEntitiesOfOrganization(newData.issueLinks,
        domainId);
    }
    if (newData.driverIds) {
      const driverRepositories = new DriverRepositories(this.contextUser);
      await driverRepositories.checkEntitiesOfOrganization(newData.driverIds, domainId as string);
    }
    if (newData.executorUserId) {
      const userRepositories = new UserRepositories(this.contextUser);
      await userRepositories.checkEntitiesOfOrganization([newData.executorUserId], 
        domainId);
    }
    if (newData.linkedSprints) {
      const sprintRepositories = new SprintRepositories(this.contextUser);
      await sprintRepositories.checkEntitiesOfOrganization(newData.linkedSprints, 
        domainId);
    }

    return !!newData;
  }

  public async getByLink(): Promise<IIssue[] | never> {
    const issueIds = await dbClient('IssueLink')
      .where('fromIssueId', this.id)
      .pluck('toIssueId');
    return this.getByIds(issueIds);
  }

  public async createIssueLink(toIssueId: string): Promise<IEntityId> {
    const data = await this.data;
    await this.checkEntitiesOfOrganization([toIssueId], data?.domainId as string);
    const [createdIssueLink] = await dbClient('IssueLink')
      .insert({
        fromIssueId: this.id,
        toIssueId,
      })
      .returning('id');
    return createdIssueLink;
  }

  public async deleteIssueLink(toIssueId: string): Promise<boolean> {
    await dbClient('IssueLink')
      .where({
        fromIssueId: this.id,
        toIssueId,
      })
      .del();
    return true;
  }

  public async changeIssueLinks(issueLinks: string[]): Promise<IEntityId> {
    const data = await this.data;
    await this.checkEntitiesOfOrganization(issueLinks, data?.domainId as string);
    const currentIssueLinks = await this.getByLink();
    const currentIssueLinksIds = currentIssueLinks.map((issue) => issue.id);
    const toAdd = issueLinks.filter((issue) => !currentIssueLinksIds.includes(issue));
    const toDelete = currentIssueLinksIds.filter((issue) => !issueLinks.includes(issue));
    for (const issue of toAdd) {
      await this.createIssueLink(issue);
    }
    for (const issue of toDelete) {
      await this.deleteIssueLink(issue);
    }
    return { id: this.id };
  }

  public async getIssueType(): Promise<IIssueType | null> {
    const data = await this.data;
    return data?.issueTypeId ? new IssueTypeRepositories(this.contextUser, data?.issueTypeId).getData() : null;
  }

  public async getStage(): Promise<IStage | null> {
    const data = await this.data;
    return data?.stageId ? new StageRepositories(this.contextUser, data?.stageId).getData() : null;
  }

  public async getAuthor(): Promise<IUser | null> {
    const data = await this.data;
    return data?.authorUserId ? new UserRepositories(this.contextUser, data?.authorUserId).getData() : null;
  }

  public async getDomain(): Promise<IDomain> {
    const data = await this.data;
    return new DomainRepositories(this.contextUser, data?.domainId).getData();
  }

  public async getExecutor(): Promise<IUser | null> {
    const data = await this.data;
    if (!data?.executorUserId) {
      return null;
    }
    return new UserRepositories(this.contextUser, data?.executorUserId).getData();
  }

  public async getDrivers(): Promise<IDriver[]> {
    const driverIssue = await dbClient('DriverIssue')
      .where('issueId', this.id)
      .select('driverId');
    const driverIds = driverIssue.map(dp => dp.driverId);
    const driverRepositories = new DriverRepositories(this.contextUser);
    return driverRepositories.getByIds(driverIds);
  }

  public async getComments() {
    return new CommentRepositories(this.contextUser).getByIssue(this.id as string);
  }
  
  public async getOfOrganization(domainId: string): Promise<IIssue[]> {
    const ids = (await this.domainHierarchy)?.getOrganizationFromDomain(domainId)?.subDomainsFlat.map((domain) => domain.id);
    return this.getByQuery((query) => query.whereIn('domainId', ids || []));
  }    

  public async close(): Promise<IIssue> {
    return this.edit({ isClosed: true, closedAt: new Date() });
  }

  public async open(): Promise<IIssue> {
    return this.edit({ isClosed: false });
  }
}