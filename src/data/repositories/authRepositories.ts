import { IUser } from '../../types/IUser';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { AuthenticationError, ForbiddenError } from 'apollo-server-express';
import { UserRepositories } from './userRepositories';
import config from '../../libs/config';
import { ILicense, LicenseRepositories } from './licenseRepositories';
import { ConfirmEmailRepositories } from './confirmEmailRepositories';

export class AuthRepositories extends UserRepositories {
  needContextUser(): boolean | undefined {
    return false;
  }

  public async signUp(email: string, name: string, password: string, license: string) {
    const user:Partial<IUser> = {
      email: '',
      confirmEmail: email.trim().toLowerCase(),
      name,
      password: password,
      license: license,
    };

    let model = await this.getByEmail(email);
    if (model) {
      throw new AuthenticationError(
        'A user with this email already exists',
      );
    }
    if (!user.password) throw new ForbiddenError('The user in the database does not have a password');
    if (config.isLicense) {
      if (!user.license) {
        throw new AuthenticationError('Лицензия не указана');
      }
      let licenseObject: ILicense;
      try {
        licenseObject = await new LicenseRepositories(undefined).getLicense(user.license);
      } catch (error) {
        throw new AuthenticationError('Лицензия не найдена');
      }
      if (licenseObject.activated) {
        throw new AuthenticationError('Лицензия уже активирована');
      }
      if (licenseObject.expireAt < new Date()) {
        throw new AuthenticationError('Лицензия истекла');
      }
      await new LicenseRepositories(undefined).activateLicense(user.license);
      user.licenseExpires = licenseObject.expireAt;
    }
    const hashed = await bcrypt.hash(user.password, 10);
    user.password = hashed;
    if (config.noConfirmation) {
      user.email = user.confirmEmail;
    }
    const createdUser = await this.create(user);
    if (!config.noConfirmation) {
      await new ConfirmEmailRepositories(this.contextUser, createdUser.id).sendEmailConfirmationLink(user.confirmEmail as string);
    }
    // await UserRepositories.SignUp(user);
    return user;
  }

  public async signIn(email: string, password: string) {
    const user:Partial<IUser> = {
      email: email,
      password: password,
    };
    if (user.email) {
      user.email = user.email.trim().toLowerCase();
    } else {
      throw new AuthenticationError('Email не указан');
    }

    let model = await this.getByEmail(user.email);
    if (!model) {
      throw new AuthenticationError('User not found');
    }
    const valid = await bcrypt.compare(user.password!, model.password!);
    if (!valid) {
      throw new AuthenticationError('Пароль неверен');
    }
    let token = jwt.sign({ id: model.id }, config.jwt.secret_key);
    return token;
  }
}
