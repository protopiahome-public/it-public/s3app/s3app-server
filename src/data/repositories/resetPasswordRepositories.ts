import crypto from 'crypto';
import { AuthenticationError } from 'apollo-server-express';
import bcrypt from 'bcrypt';
import { UserRepositories } from './userRepositories';
import { transporter } from '../../utils/email';
import config from '../../libs/config';
import dayjs from 'dayjs';
import { IUserResDTO } from '../../types/DTO/IUserResDTO';

export class ResetPasswordRepositories extends UserRepositories {
  needContextUser(): boolean | undefined {
    return false;
  }
  
  public async sendRecoverPasswordLink(email: string) {
    let model = await this.getByEmail(email);
    if (!model) {
      throw new AuthenticationError('User not found');
    }
    const resetPasswordCode = crypto
      .getRandomValues(new Uint32Array(1))[0]
      .toString(16);
    const resetPasswordExpires = Date.now() + 3600000;
    model.resetPasswordCode = resetPasswordCode;
    model.resetPasswordExpires = new Date(resetPasswordExpires);
    if (model.id) {
      await new ResetPasswordRepositories(undefined, model.id).edit(model);
    }
    const result = await transporter.sendMail({
      from: `S3APP <${config.emailTransporter?.auth.user}>`,
      to: email,
      subject: 'Ссылка на восстановление пароля',
      text: `Ссылка на восстановление пароля: ${config.host}/restore-password/confirm/?code=${resetPasswordCode}`,
    });
    if (result) {
      return true;
    }
    return false;
  }
    
  public async checkResetPasswordCode(code: string) {
    const model = (await this.getByFields({ resetPasswordCode: code }))[0];
    if (!model) {
      throw new AuthenticationError('Временный код не найден');
    }
    const currTime = dayjs();
    const diffTime = currTime.diff(dayjs(model.resetPasswordExpires), 'minute');
    if (
      model.resetPasswordExpires &&
                diffTime > 60
    ) {
      throw new AuthenticationError('Токен истек');
    }
    const userDTO: IUserResDTO = {
      id: model._id?.toString() ?? '',
      name: model.name,
      email: model.email,
    };
    return userDTO;
  }
    
  public async changePasswordAfterRecover(code: string, password: string) {
    const model = (await this.getByFields({ resetPasswordCode: code }))[0];
    if (!model) {
      throw new AuthenticationError('Неверный код подтверждения');
    }
    const hashed = await bcrypt.hash(password, 10);
    model.password = hashed;
    if (model.resetPasswordExpires && model.resetPasswordExpires) {
      model.resetPasswordCode = '';
      model.resetPasswordExpires = undefined;
    }
    if (model.id) {
      await new ResetPasswordRepositories(undefined, model.id).edit(model);
    }
    return true;
  }

}