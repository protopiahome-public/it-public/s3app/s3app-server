import { IEntity } from './IEntity';

export interface ISprint extends IEntity {
  id: string,
  name: string,
  goal: string,
  startDate: Date,
  endDate: Date,
  participants?: string[],
  issues?: string[],
  planningEventId?: string,
  reviewEventId?: string,
  domainId: string,
  endDateForPlanningEvent?: Date,
  endDateForReviewEvent?: Date,
}