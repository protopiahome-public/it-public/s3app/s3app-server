export interface IUserResDTO {
  id: string;
  name: string;
  email: string;
}
