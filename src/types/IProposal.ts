import { IEntity } from './IEntity';

enum ProposalStatus {
  new = 'new', 
  accepted = 'accepted', 
  holded = 'holded', 
  rejected = 'rejected',
  ceased = 'ceased',
  review = 'review',
}

export interface IProposal extends IEntity {
  authorUserId: string,
  status: ProposalStatus,
  description: string,
  driverIds?: string[],
  domainId: string,
  previousProposalId?: string,
}