import { IEntity } from './IEntity';

enum TensionMark {
  positive = '+',
  negative = '-',
  neutral = '0',
}

export interface ITension extends IEntity {
  authorUserId: string,
  tensionMark?: TensionMark,
  description: string,
  domainId: string,
}