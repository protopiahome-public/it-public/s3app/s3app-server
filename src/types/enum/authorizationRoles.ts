export enum AuthorizationRoles {
  'admin' = 'admin',
  'moderator' = 'moderator',
  'user' = 'user',
}