import { IEntity } from './IEntity';

export interface IIssueType extends IEntity {
  authorUserId: string,
  description: string,
  stageIds?: string[],
  domainId: string,
}