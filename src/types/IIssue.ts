import { IEntity } from './IEntity';

export interface IIssue extends IEntity {
  id: string,
  domainId?: string,
  authorUserId?: string,
  name?: string,
  description?: string,
  role?: string,
  priority?: number,
  issueLinks?: string[],
  executorUserId?: string,
  sprintId?: string | null,
  linkedSprints?: string[],
  stageId?: string,
  issueTypeId?: string,
  historyLog?: string[],
  comments?: string[],
  version?: string,
  isClosed?: boolean,
  closedAt?: Date,
  deadlineAt?: Date,
  driverIds?: string[],
}