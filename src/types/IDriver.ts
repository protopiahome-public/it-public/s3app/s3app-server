import { IEntity } from './IEntity';

enum DriverEffect {
  positive = '+',
  negative = '-',
  neutral = '0',
}

enum DriverState {
  need = 'need',
  goal = 'goal',
  satisfied = 'satisfied',
  irrelevent = 'irrelevent',
}

export interface IDriver extends IEntity {
  authorUserId: string,
  description: string,
  tensionIds?: string[],
  domainId: string,
  driverEffect?: DriverEffect,
  driverState?: DriverState,
}