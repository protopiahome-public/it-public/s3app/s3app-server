import { IEntity } from './IEntity';

export enum EventType {
  'planning' = 'planning',
  'review' = 'review',
  'retrospective' = 'retrospective',
  'management' = 'management',
  'choiceForRole' = 'choiceForRole',
}

export interface IEvent extends IEntity {
  id: string,
  name: string,
  domainId?: string,
  type: EventType,
  startDate: Date,
  endDate: Date,
  participants?: string[],
  facilitatorId?: string,
}