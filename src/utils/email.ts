import nodemailer from 'nodemailer';
import config from '../libs/config';

export const transporter = nodemailer.createTransport({
  host: config.emailTransporter?.host,
  port: config.emailTransporter?.port,
  auth: {
    user: config.emailTransporter?.auth.user,
    pass: config.emailTransporter?.auth.pass,
  },
});
