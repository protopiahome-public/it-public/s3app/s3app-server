export interface Config {
  port: number;
  host: string;
  mongoose: {
    uri: string;
    options: {
      keepAlive: boolean;
      maxPoolSize: number;
    };
  };
  emailTransporter?: {
    host: string;
    port: number;
    auth: {
      user: string;
      pass: string;
    };
  };
  noConfirmation?: boolean;
  jwt: {
    secret_key: string;
  };
  db: {
    client: string;
    connection: {
      connectionString: string;
    };
    pool?: {
      min: number;
      max: number;
      acquireTimeoutMillis: number;
      idleTimeoutMillis: number;
    }
  };
  isDemo: boolean;
  demoUser: string;
  isLicense: boolean;
  licenseRepository: string;
  licenseRepositoryAccessToken: string;
  oneOrganization: boolean;
}

const config = {
  port: 3000,
  host: 'http://test.ru',
  mongoose: {
    uri: 'Строчка uri для подключения к базе данных',
    options: {
      keepAlive: true,
      maxPoolSize: 10,
    },
  },
  emailTransporter: {
    host: 'Адрес хоста для емейл-транспортёра',
    port: 587,
    auth: {
      user: 'Логин',
      pass: 'Пароль',
    },
  },
  jwt: {
    secret_key: '',
  },
  db: {
    client: 'pg',
    connection: {
      connectionString: 'postgres://postgres:1234@localhost:5432/s3app',
    },
  },
  isDemo: false,
  demoUser: '',
  isLicense: false,
  licenseRepository: '',
  licenseRepositoryAccessToken: '',
  oneOrganization: false,
};

export default config;
