import { Config } from './config.sample';

const config: Config = {
  port: parseInt(process.env.S3APP_PORT || '') || 3000,
  host: process.env.S3APP_HOST || 'https://test.ru',
  mongoose: {
    'uri': process.env.S3APP_MONGO_URI || 'Строчка uri для подключения к базе данных',
    'options': {
      'keepAlive': !!process.env.S3APP_MONGO_KEEP_ALIVE || true,
      'maxPoolSize': parseInt(process.env.S3APP_MONGO_POOL_SIZE || '') || 10,
    },
  },
  emailTransporter: {
    host: process.env.S3APP_EMAIL_HOST || 'Адрес хоста для емейл-транспортёра',
    port: parseInt(process.env.S3APP_EMAIL_PORT || '') || 587,
    auth: {
      user: process.env.S3APP_EMAIL_USER || 'Логин',
      pass: process.env.S3APP_EMAIL_PASSWORD || 'Пароль',
    },
  },
  noConfirmation: process.env.S3APP_NO_CONFIRMATION ? true : false,
  jwt: {
    secret_key: process.env.S3APP_JWT_SECRET_KEY || '',
  },
  db: {
    client: 'pg',
    connection: {
      connectionString: process.env.S3APP_DB_POSTGRES || '',
    },
    pool: {
      min: 0,
      max: 10,
      acquireTimeoutMillis: 60000,
      idleTimeoutMillis: 600000,
    },
  },
  isDemo: process.env.S3APP_DEMO ? true : false,
  demoUser: process.env.S3APP_DEMO_USER || '',
  isLicense: process.env.S3APP_LICENSE ? true : false,
  licenseRepository: process.env.S3APP_LICENSE_REPOSITORY || '',
  licenseRepositoryAccessToken: process.env.S3APP_LICENSE_REPOSITORY_ACCESS_TOKEN || '',
  oneOrganization: process.env.S3APP_ONE_ORGANIZATION ? true : false,
};


export = config;
