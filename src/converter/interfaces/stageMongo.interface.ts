import { IStage } from '../../types/IStage';

export interface IStageMongo extends IStage {
  _id: string,
  mainDomain: string,
  deleted: string
}