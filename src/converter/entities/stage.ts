import { model, Schema, Types } from 'mongoose';
import { IStageMongo } from '../interfaces/stageMongo.interface';

const schema = new Schema(
  {
    name: String,
    mainDomain: { type: Types.ObjectId, ref: 'domain' },
    stageDomain: { type: Types.ObjectId, ref: 'domain' },
  },
);


export = model<IStageMongo>('stage', schema);
