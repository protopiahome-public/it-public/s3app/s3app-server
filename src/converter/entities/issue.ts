import { model, Schema, Types } from 'mongoose';
import { IIssueMongo } from '../interfaces/issueMongo.interface';

const schema = new Schema({
  domain: {
    type: Types.ObjectId,
    ref: 'domain',
  },
  author: {
    type: Types.ObjectId,
    ref: 'user',
  },
  name: String,
  description: String,
  role: {
    type: Types.ObjectId,
    ref: 'role',
  },
  dateCreated: {
    type: Date,
  },
  priority: Number,
  issueLinks: [{
    type: Types.ObjectId,
    ref: 'issue',
  }],
  executor: {
    type: Types.ObjectId,
    ref: 'user',
  },
  sprint: {
    type: Types.ObjectId,
    ref: 'sprint',
  },
  linkedSprints: [{
    type: Types.ObjectId,
    ref: 'sprint',
  }],
  stage: {
    type: Types.ObjectId,
    ref: 'stage',
  },
  historyLog: {
    type: Types.ObjectId,
    ref: 'history',
  },
  comments: [{
    type: Types.ObjectId,
    ref: 'comment',
  }],
  version: String,
});


export = model<IIssueMongo>('issue', schema);


