import { Schema, Types } from 'mongoose';
import mongoose_delete from 'mongoose-delete';

const schema = new Schema({
  description: String,
  status: {
    type: String,
    enum: [
      'accepted',
      'notAccepted',
      'consideration',
    ],
  },
  domain: {
    type: Types.ObjectId,
  },
});

schema.plugin(mongoose_delete, { overrideMethods: 'all' });

export = schema;