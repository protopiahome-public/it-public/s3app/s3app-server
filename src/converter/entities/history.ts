import { model, Schema } from 'mongoose';
import mongoose_delete from 'mongoose-delete';
import { IHistory } from '../../types/IHistory';

const schema = new Schema({
  name: String,
  description: String,
  date: Date,
  type: {
    type: String,
    enum: [
      'create',
      'update',
      'newExecutor',
      'executorStartedWork',
      'newSprints',
    ],
  },
});

schema.plugin(mongoose_delete, { overrideMethods: 'all' });

export = model<IHistory>('history', schema);