import { model, Schema } from 'mongoose';
import { IUserMongo } from '../interfaces/userMongo.interface';

const schema = new Schema({
  name: String,
  email: String,
  password: String,
  resetPasswordExpires: String,
  resetPasswordCode: String,
  confirmEmail: String,
  confirmEmailExpires: {
    type: Date,
    default: null,
  },
  confirmEmailCode: String,
  authorizationRole: [String],
  isDeleted: {
    type: Boolean,
    default: false,
  },
  deletedAt: {
    type: Date,
    default: null,
  },
});


export = model<IUserMongo>('user', schema);