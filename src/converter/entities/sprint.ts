import { model, Schema, Types } from 'mongoose';
import { ISprintMongo } from '../interfaces/sprintMongo.interface';

const schema = new Schema({
  name: {
    type: String,
  },
  goal: {
    type: String,
  },
  startDate: {
    type: Date,
  },
  endDate: {
    type: Date,
  },
  participants: [{
    type: Types.ObjectId,
    ref: 'user',
  }],
  planningEvent: {
    type: Types.ObjectId,
    ref: 'event',
  },
  reviewEvent: {
    type: Types.ObjectId,
    ref: 'event',
  },
  domain: {
    type: Types.ObjectId,
    ref: 'domain',
  },
});


export = model<ISprintMongo>('sprint', schema);