import issueModel from './entities/issue';
import userModel from './entities/user';
import sprintModel from './entities/sprint';
import eventModel from './entities/event';
import areaOfInfluenceSchema from './entities/areaOfInfluence';
import stageModel from './entities/stage';
import { v4 as uuidv4 } from 'uuid';
import dbClient from '../utils/knex';
import mongoose, { model } from 'mongoose';
import { IIssueMongo } from './interfaces/issueMongo.interface';
import { IUserMongo } from './interfaces/userMongo.interface';
import { IDomainMongo } from './interfaces/domainMongo.interface';
import { IEventMongo } from './interfaces/eventMongo.interface';
import { ISprintMongo } from './interfaces/sprintMongo.interface';
import { IStageMongo } from './interfaces/stageMongo.interface';
import dayjs from 'dayjs';
import config from '../libs/config';

const domainModel = model<IDomainMongo>('domain', areaOfInfluenceSchema);

type MongoType<T> = (
    mongoose.Document<unknown, any, T>
    & T
    & Required<{
      _id: string;
    }>
);

type MapIds = {
  pgId: string,
  mongoId: string
};

type AllMapIdsType = {
  issueMapIds: MapIds[];
  userMapIds: MapIds[];
  domainMapIds: MapIds[];
  eventMapIds: MapIds[];
  sprintMapIds: MapIds[];
  stageMapIds: MapIds[];
};



function findPgId(mongoId: string, mapIds: MapIds[]) {
  const pgId = mapIds.find(map => map.mongoId === mongoId);
  if (pgId) {
    return pgId.pgId;
  }
}
  
function returnMapIds(collectionData: mongoose.Document<any>[]) {
  const mapIds: MapIds[] = [];
  for (const item of collectionData) {
    mapIds.push({
      pgId: uuidv4(),
      mongoId: item._id.toString(),
    });
  }
  return mapIds;
}
  
async function deleteAllData() {
  await dbClient('Issue').del();
  await dbClient('IssueLink').del();
  await dbClient('User').del();
  await dbClient('Event').del();
  await dbClient('Stage').del();
  await dbClient('UserSprint').del();
  await dbClient('Sprint').del();
  await dbClient('EventParticipant').del();
  await dbClient('Domain').del();
}

async function convertStages(
  stagesData: MongoType<IStageMongo>[],
  allMapIds: AllMapIdsType,
) {
  console.log('The Stages conversion has begun');

  for (const stageDoc of stagesData) {
    const stage = stageDoc.toObject();
    const pgId = findPgId(stage._id!.toString(), allMapIds.stageMapIds);
    const stagePgAdd = {
      id: pgId,
      name: stage.name ?? '',
      domainId: findPgId(stage.mainDomain.toString(), allMapIds.domainMapIds),
      isDeleted: stage.deleted ?? false,
    };
    try {
      await dbClient('Stage').insert(stagePgAdd);
    } catch (err) {
      console.log(err);
      console.log(stagePgAdd);

    }
  }
  console.log('The Stages conversion is complete');
}

async function convertSprints(
  sprintsData: MongoType<ISprintMongo>[],
  allMapIds: AllMapIdsType,
) {
  console.log('The Sprints conversion has begun');

  const { sprintMapIds, userMapIds, eventMapIds, domainMapIds } = allMapIds;
  for (const sprintDoc of sprintsData) {
    const sprint = sprintDoc.toObject();
    const pgId = findPgId(sprint._id!.toString(), sprintMapIds);
    if (sprint.participants?.length) {
      for (const participant of sprint.participants) {
        const userPgId = findPgId(participant.toString(), userMapIds);
        const userEventLink = {
          userId: userPgId,
          sprintId: pgId,
        };
        await dbClient('UserSprint').insert(userEventLink);
      }
    }
    const sprintPgAdd = {
      id: pgId,
      name: sprint.name ?? '',
      goal: sprint.goal ?? '',
      startDate: sprint.startDate,
      endDate: sprint.endDate,
      planningEventId: sprint.planningEvent ? findPgId(sprint.planningEvent.toString(), eventMapIds) : null,
      reviewEventId: sprint.reviewEvent ? findPgId(sprint.reviewEvent.toString(), eventMapIds) : null,
      domainId: sprint.domain ? findPgId(sprint.domain.toString(), domainMapIds) : null,
      isDeleted: sprint.deleted ?? false,
    };
    try {
      await dbClient('Sprint').insert(sprintPgAdd);
    } catch (err) {
      console.log(err);
      console.log(sprintPgAdd);
    }
  }
  console.log('The Sprints conversion is complete');

}

async function convertEvents(
  eventsData: MongoType<IEventMongo>[],
  allMapIds: AllMapIdsType,
) {
  console.log('The Events conversion has begun');

  const { eventMapIds, userMapIds, domainMapIds } = allMapIds;
  for (const eventDoc of eventsData) {
    const event = eventDoc.toObject();
    const pgId = findPgId(event._id!.toString(), eventMapIds);
    if (event.participants?.length) {
      for (const participant of event.participants) {
        const userPgId = findPgId(participant.toString(), userMapIds);
        const userEventLink = {
          userId: userPgId,
          eventId: pgId,
        };
        await dbClient('EventParticipant').insert(userEventLink);
      }
    }
    const eventPgAdd = {
      id: pgId,
      name: event.name ?? '',
      domainId: event.domain ? findPgId(event.domain.toString(), domainMapIds) : null,
      type: event.type,
      startDate: event.startDate ?? null,
      endDate: event.endDate ?? null,
    };
    await dbClient('Event').insert(eventPgAdd);
  }
  console.log('The Events conversion is complete');

}


async function convertDomains(
  domainData: MongoType<IDomainMongo>[],
  allMapIds: AllMapIdsType,
) {
  console.log('The Domains conversion has begun');

  const { domainMapIds, userMapIds } = allMapIds;
  for (const domainDoc of domainData) {
    const domain = domainDoc.toObject();
    const pgId = findPgId(domain._id!.toString(), domainMapIds);
    if (domain.participants?.length) {
      for (const participant of domain.participants) {
        const userPgId = findPgId(participant.toString(), userMapIds);
        const userDomainLink = {
          userId: userPgId,
          domainId: pgId,
          linkType: 'consist',
        };
        await dbClient('UserDomainLink').insert(userDomainLink);
      }
    }
    const domainType = domain.isOrganization ? 'organization' : 'circle';
    const domainPgAdd = {
      id: pgId,
      name: domain.name ?? '',
      isDeleted: domain.deleted ?? false,
      // deletedAt: domain.deletedAt,
      // isOrganization: domain.isOrganization ?? false,
      domainType,
      parentDomainId: domain.mainDomain ? findPgId(domain.mainDomain.toString(), domainMapIds) : null,

    };
    await dbClient('Domain').insert(domainPgAdd);
  }
  console.log('The Domains conversion is complete');

}

async function convertUsers(
  usersData: MongoType<IUserMongo>[],
  allMapIds: AllMapIdsType,
//   domainData: MongoType<IDomainMongo>[],
) {
  const { userMapIds } = allMapIds;
  console.log('The Users conversion has begun');

  for (const userDoc of usersData) {
    const user = userDoc.toObject();
    const mongoId = user._id!.toString();
    const pgId = findPgId(mongoId, userMapIds);
    // const mainDomainId = (() => {
    //     const domain = domainData.filter(domain => domain.participants!.includes(mongoId));
    //     const domainPgId = findPgId(domain[0]._id!.toString(), domainMapIds)
    //     return domainPgId
    // })()
    const userPgAdd = {
      id: pgId,
      name: user.name ?? '',
      email: user.email ?? '',
      password: user.password ?? '',
      isDeleted: user.deleted ?? false,
      deletedAt: user.deletedAt ?? null,
      // domainId: user.mainDomain ?? null,
      confirmEmail: user.confirmEmail ?? '',
      confirmEmailExpires: user.confirmEmailExpires ?? null,
      confirmEmailCode: user.confirmEmailCode ?? '',
      resetPasswordExpires: user.resetPasswordExpires ? dayjs(user.resetPasswordExpires).toISOString() : null,
      resetPasswordCode: user.resetPasswordCode ?? '',
    };
    await dbClient('User').insert(userPgAdd);
  }
  console.log('The Users conversion is complete');
}

async function convertIssues(
  issuesData: MongoType<IIssueMongo>[],
  allMapIds: AllMapIdsType,
) {
  console.log('The Issues conversion has begun');

  const { issueMapIds, userMapIds, domainMapIds, stageMapIds, sprintMapIds } = allMapIds;

  for (const issueDoc of issuesData) {
    const issue = issueDoc.toObject();
    // const mongoId = issue._id!.toString()
    const pgId = findPgId(issue._id!.toString(), issueMapIds);
    if (issue.issueLinks.length) {
      for (const link of issue.issueLinks) {
        const toIssueId = findPgId(link.toString(), issueMapIds);
        await dbClient('IssueLink').insert({
          fromIssueId: pgId,
          toIssueId,
        });
      }
    }

    const pgIssueAdd = {
      id: pgId,
      authorUserId: issue.author ? findPgId(issue.author.toString(), userMapIds) : null,
      domainId: issue.domain ? findPgId(issue.domain.toString(), domainMapIds) : null,
      executorUserId: issue.executor ? findPgId(issue.executor.toString(), userMapIds) : null,
      stageId: issue.stage ? findPgId(issue.stage.toString(), stageMapIds) : null,
      sprintId: issue.sprint ? findPgId(issue.sprint.toString(), sprintMapIds) : null,
      name: issue.name ?? '',
      description: issue.description ?? '',
      priority: issue.priority ?? 0,
      isDeleted: issue.deleted ?? false,
      createdAt: issue.dateCreated ?? dayjs().toISOString(),
      // mongoId
    };

    await dbClient('Issue').insert(pgIssueAdd);
  }
  console.log('The Issues conversion is complete');

}

export const startConverting = async () => {
  console.log('Start converting');
  await dbClient.raw('set session_replication_role = replica;');
  console.log('Disabled constraint for postgres');
  await deleteAllData();
  console.log('Deleted all data');
  await mongoose
    .connect(config.mongoose.uri, {
      // @ts-ignore
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
  
  const issuesData = await issueModel.find({}).exec();
  const usersData = await userModel.find({}).exec();
  const domainsData = await domainModel.find({}).exec();
  const eventsData = await eventModel.find({}).exec();
  const sprintsData = await sprintModel.find({}).exec();
  const stagesData = await stageModel.find({}).exec();
  console.log('Uploaded all the entities from Mongo');
  
  const issueMapIds: MapIds[] = returnMapIds(issuesData);
  const userMapIds: MapIds[] = returnMapIds(usersData);
  const domainMapIds: MapIds[] = returnMapIds(domainsData);
  const eventMapIds: MapIds[] = returnMapIds(eventsData);
  const sprintMapIds: MapIds[] = returnMapIds(sprintsData);
  const stageMapIds: MapIds[] = returnMapIds(stagesData);
  const allMapIds = {
    issueMapIds,
    userMapIds,
    domainMapIds,
    eventMapIds,
    sprintMapIds,
    stageMapIds,
  };
  
  await convertIssues(issuesData, allMapIds);
  await convertUsers(usersData, allMapIds/*, domainsData */);
  await convertDomains(domainsData, allMapIds);
  await convertEvents(eventsData, allMapIds);
  await convertSprints(sprintsData, allMapIds);
  await convertStages(stagesData, allMapIds);
  process.exit();
};

startConverting();