import { SprintRepositories } from '../../../data/repositories/sprintRepositories';
import { IDomain } from '../../../types/IDomain';
import { IEntityId } from '../../../types/IEntityId';
import { IEvent } from '../../../types/IEvent';
import { IIssue } from '../../../types/IIssue';
import { ISprint } from '../../../types/ISprint';
import { IUser } from '../../../types/IUser';
import { BaseResolver } from './baseResolvers';

// export const sprintResolvers = () => {

//   return {
//     Query: {
//       getSprints: () => SprintServices.getAll(),
//       getSprint: async (_: object, args: { id: string }, context: { user: IUser }) => {
//         const sprint = await SprintServices.getData(args.id);
//         if (sprint && !await DomainHierarchy.checkUserDomain(context.user.id, sprint.domainId)) {
//           throw new Error('Access denied');
//         }
//         return sprint;
//       },
//     },
//     Mutation: {
//       createSprint: async (_: object, args: { sprint: ISprint }) => SprintServices.createSprint(args.sprint),
//       deleteSprint: async (_: object, args: { id: string }) => SprintServices.delete(args.id),
//       editSprint: async (_: object, args: { id: string, sprint: ISprint }) => SprintServices.edit(args.id, args.sprint),
//       addIssuesToSprint: async (_: object, args: { sprintId: string, issueIds: string[] }) => SprintServices.changeIssuesForSpint(args.sprintId, args.issueIds),
//       removeIssuesFromSprint: async (_: object, args: { issueIds: string[] }) => SprintServices.changeIssuesForSpint(null, args.issueIds),
//       addUsersToSprint: async (_: object, args: { id: string, userIds: string[] }) => SprintServices.addUsers(args.id, args.userIds),
//       removeUsersFromSprint: async (_: object, args: { id: string, userIds: string[] }) => SprintServices.removeUsers(args.id, args.userIds),
//     },
//     Sprint: {
//       planningEvent: async (sprint: ISprint) => sprint.planningEventId ? EventServices.getData(sprint.planningEventId) : [], //TODO: проверить во время рефакторинга спринта
//       reviewEvent: async (sprint: ISprint) => sprint.reviewEventId ? EventServices.getData(sprint.reviewEventId) : [],
//       participants: async (sprint: ISprint) => sprint.id ? SprintServices.getParticipants(sprint.id) : [],
//       issues: async (sprint: ISprint) => sprint.id ? IssueServices.getBySprint(sprint.id) : null,
//     },
//   };
// };

class SprintResolvers extends BaseResolver {
  setResolvers(): void {
    this.addQueryResolver<{}, ISprint[]>('getSprints', 
      (args, user) => new SprintRepositories(user).getAll());

    this.addQueryResolver<{ id: string }, ISprint | null>('getSprint', 
      async (args, user) => new SprintRepositories(user, args.id).getData());

    this.addMutationResolver<{ sprint: ISprint }, IEntityId>('createSprint', 
      async (args, user) => new SprintRepositories(user).create(args.sprint));

    this.addMutationResolver<{ id: string }, boolean>('deleteSprint',
      async (args, user) => new SprintRepositories(user, args.id).delete());

    this.addMutationResolver<{ id: string, sprint: ISprint }, ISprint>('editSprint',
      async (args, user) => new SprintRepositories(user, args.id).edit(args.sprint));

    this.addMutationResolver<{ sprintId: string, issueIds: string[] }, boolean>('addIssuesToSprint',
      async (args, user) => new SprintRepositories(user, args.sprintId).changeIssuesForSpint(args.issueIds));

    this.addMutationResolver<{ issueIds: string[] }, boolean>('removeIssuesFromSprint',
      async (args, user) => new SprintRepositories(user).changeIssuesForSpint(args.issueIds));

    this.addMutationResolver<{ id: string, userIds: string[] }, boolean>('addUsersToSprint',
      async (args, user) => new SprintRepositories(user, args.id).addUsers(args.userIds));

    this.addMutationResolver<{ id: string, userIds: string[] }, boolean>('removeUsersFromSprint',
      async (args, user) => new SprintRepositories(user, args.id).removeUsers(args.userIds));

    this.addEntityResolver<ISprint, IEvent | null>('Sprint', 'planningEvent', 
      async (sprint, user) => new SprintRepositories(user, sprint).getPlanningEvent());

    this.addEntityResolver<ISprint, IEvent | null>('Sprint', 'reviewEvent',
      async (sprint, user) => new SprintRepositories(user, sprint).getReviewEvent());

    this.addEntityResolver<ISprint, IUser[]>('Sprint', 'participants',
      async (sprint, user) => new SprintRepositories(user, sprint).getParticipants());

    this.addEntityResolver<ISprint, IIssue[]>('Sprint', 'issues',
      async (sprint, user) => new SprintRepositories(user, sprint).getIssues());
    
    this.addEntityResolver<ISprint, IDomain>('Sprint', 'domain',
      async (sprint, user) => new SprintRepositories(user, sprint).getDomain());
  }
}

export const sprintResolvers = new SprintResolvers();