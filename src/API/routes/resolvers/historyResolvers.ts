import { HistoryRepositories } from '../../../data/repositories/historyRepositories';
import { IEntityId } from '../../../types/IEntityId';
import { IHistory } from '../../../types/IHistory';
import { BaseResolver } from './baseResolvers';

// export const historyResolvers = () => {

//   return {
//     Query: {
//       getHistory: async (_: object, args: { id: string }) => HistoryServices.getData(args.id),
//       getHistories: async () => HistoryServices.getAll(),
//     },
//     Mutation: {
//       createHistory: async (_: object, args: { history: IHistory }) => HistoryServices.create(args.history),
//       editHistory: async (_: object, args: { id: string, history: IHistory }) => HistoryServices.edit(args.id, args.history),
//       deleteHistory: async (_: object, args: { id: string }) => HistoryServices.delete(args.id),
//     },
//   };
// };

class HistoryResolvers extends BaseResolver {
  setResolvers() {
    this.addQueryResolver<{ id: string }, IHistory | null>('getHistory',
      async (args, user) => new HistoryRepositories(user, args.id).getData());

    this.addQueryResolver<{}, IHistory[]>('getHistories',
      async (args, user) => new HistoryRepositories(user).getAll());

    this.addMutationResolver<{ history: IHistory }, IEntityId>('createHistory',
      async (args, user) => new HistoryRepositories(user).create(args.history));

    this.addMutationResolver<{ id: string, history: IHistory }, IHistory>('editHistory',
      async (args, user) => new HistoryRepositories(user, args.id).edit(args.history));

    this.addMutationResolver<{ id: string }, boolean>('deleteHistory',
      async (args, user) => new HistoryRepositories(user, args.id).delete());
  }
}

export const historyResolvers = new HistoryResolvers();