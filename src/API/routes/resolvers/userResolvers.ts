import { UserRepositories } from '../../../data/repositories/userRepositories';
import { IComment } from '../../../types/IComment';
import { IDomain } from '../../../types/IDomain';
import { IEntityId } from '../../../types/IEntityId';
import { IEvent } from '../../../types/IEvent';
import { IIssue } from '../../../types/IIssue';
import { ISprint } from '../../../types/ISprint';
import { IUser } from '../../../types/IUser';
import { BaseResolver } from './baseResolvers';

// export const userResolvers = () => {
//   return {
//     Query: {
//       getUsers: () => UserServices.getAll(),
//       getUser: async (_: object, args: { id: string }) =>
//         UserServices.getData(args.id),
//       me: async (_: object, args: object, context: { user: IUser }) => {
//         if (!context.user) return null;
//         return context.user;
//       },
//     },

//     Mutation: {
//       createUser: async (_: object, args: { user: IUser }) =>
//         UserServices.createUser(args.user),
//       editUser: async (_: object, args: { id: string; user: IUser }) =>
//         UserServices.editUser(args.id, args.user),
//       deleteUser: async (_: object, args: { id: string }) =>
//         UserServices.delete(args.id),
//     },
//     User: {
//       executorInIssues: async (user: IUser) => {
//         if (user.id)
//           return IssueServices.getByExecutorUser(
//             user.id,
//           ); //TODO: протестировать при Issues
//       },
//       authorInIssues: async (user: IUser) => {
//         if (user.id)
//           return IssueServices.getByAuthorUser(
//             user.id,
//           ); //TODO: протестировать при Issues
//       },
//       participantInEvents: async (user: IUser) => {
//         if (user.id)
//           return UserServices.getPartiticpantEvents(user.id);

//       },
//       facilitatorInEvents: async (user: IUser) => {
//         if (user.id)
//           return UserServices.getFacilitatorEvents(user.id);
//       },
//       domains: async (user: IUser) => {
//         if (user.id)
//           return UserServices.getUserDomains(user.id);
//       },
//       sprints: async (user: IUser) => {
//         if (user.id)
//           return SprintServices.getByUser(user.id);
//       },
//       comments: async (user: IUser) => {
//         if (user.id) return CommentServices.getByUserId(user.id);
//       },
//     },
//   };
// };

class UserResolvers extends BaseResolver {
  setResolvers(): void {
    this.addQueryResolver<{}, IUser[]>('getUsers', 
      (args, user) => new UserRepositories(user).getAll());

    this.addQueryResolver<{ id: string }, IUser | null>('getUser', 
      async (args, user) => new UserRepositories(user, args.id).getData());

    this.addQueryResolver<{}, IUser | null>('me', async (args, user) => {
      if (!user) return null;
      return user;
    });

    this.addQueryResolver<{ domainId: string }, IUser[]>('getUsersOfOrganization',
      async (args, user) => new UserRepositories(user).getOfOrganization(args.domainId));

    // this.addMutationResolver<{ user: IUser }, IEntityId>('createUser', 
    //   async (args, user) => new UserRepositories(user).create(args.user));

    this.addMutationResolver<{ id: string, user: IUser }, IEntityId>('editUser', 
      async (args, user) => new UserRepositories(user, user.id).edit(args.user));

    // this.addMutationResolver<{ id: string }, boolean>('deleteUser',
    //   async (args, user) => new UserRepositories(user, args.id).delete());

    this.addEntityResolver<IUser, IIssue[]>('User', 'executorInIssues', 
      async (user, contextUser) => new UserRepositories(contextUser, user.id).getExecutorInIssues());

    this.addEntityResolver<IUser, IIssue[]>('User', 'authorInIssues', 
      async (user, contextUser) => new UserRepositories(contextUser, user.id).getAuthorInIssues());

    this.addEntityResolver<IUser, IEvent[]>('User', 'participantInEvents', 
      async (contextUser, user) => new UserRepositories(contextUser, user).getPartiticpantEvents());

    this.addEntityResolver<IUser, IEvent[]>('User', 'facilitatorInEvents', 
      async (contextUser, user) => new UserRepositories(contextUser, user).getFacilitatorEvents());

    this.addEntityResolver<IUser, IDomain[]>('User', 'domains', 
      async (user, contextUser) => new UserRepositories(contextUser, user.id).getUserDomains());

    this.addEntityResolver<IUser, ISprint[]>('User', 'sprints', 
      async (user, contextUser) => new UserRepositories(contextUser, user.id).getSprints());

    this.addEntityResolver<IUser, IComment[]>('User', 'comments', 
      async (user, contextUser) => new UserRepositories(contextUser, user.id).getComments());
  }
}

export const userResolvers = new UserResolvers();
