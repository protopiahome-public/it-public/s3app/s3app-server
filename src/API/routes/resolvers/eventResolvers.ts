import { EventRepositories } from '../../../data/repositories/eventRepositories';
import { IDomain } from '../../../types/IDomain';
import { IEvent } from '../../../types/IEvent';
import { IUser } from '../../../types/IUser';
import { BaseResolver } from './baseResolvers';

// export const eventResolvers = () => {

//   return {
//     Query: {
//       getEvents: () => EventServices.getAll(),
//       getEvent: async (_: object, args: { id: string }, context: { user: IUser }) => {
//         const event = await EventServices.getData(args.id);
//         if (event && !await DomainHierarchy.checkUserDomain(context.user.id, event.domainId)) {
//           throw new Error('Access denied');
//         }
//         return event;
//       },
//     },
//     Mutation: {
//       createEvent: resolver<{ event: IEvent }, Promise<IEvent>>(
//         async (_, args, context) => {
//           if (args.event.domainId && !await DomainHierarchy.checkUserDomain(args.event.domainId, context.user.id)) {
//             throw new Error('Access denied');
//           }
//           return EventServices.createEvent(args.event);
//         },
//       ),
//       editEvent: resolver<{ id: string, event: IEvent }, Promise<IEvent>>(
//         async (_, args, context) => {
//           if (!await DomainHierarchy.checkUserEvent(context.user.id, args.id)) {
//             throw new Error('Access denied');
//           }
//           if (args.event.domainId && !await DomainHierarchy.checkUserDomain(context.user.id, args.event.domainId)) {
//             throw new Error('Access denied');
//           }
//           return EventServices.edit(args.id, args.event);
//         },
//       ),
//       deleteEvent: resolver<{ id: string }, Promise<Boolean>>(
//         async (_, args, context) => {
//           if (!await DomainHierarchy.checkUserEvent(context.user.id, args.id)) {
//             throw new Error('Access denied');
//           }
//           return EventServices.delete(args.id);
//         },
//       ),
//       addParticipantsInEvent: async (_: object, args: { eventId: string, userIds: string[] }) => EventServices.addParticipants(args.eventId, args.userIds),
//       removeParticipantsFromEvent: async (_: object, args: { eventId: string, userIds: string[] }) => EventServices.removeParticipants(args.eventId, args.userIds),
//     },
//     Event: {
//       domain: async (event: IEvent) => event.domainId ? DomainServices.getData(event.domainId) : null,
//       participants: async (event: IEvent) => event.id ? EventServices.getParticipants(event.id) : [],
//       facilitator: async (event: IEvent) => event.facilitatorId ? UserServices.getData(event.facilitatorId) : null,
//     },
//   };
// };

class EventResolvers extends BaseResolver {
  setResolvers() {
    this.addQueryResolver<{}, IEvent[]>('getEvents', 
      async (args, user) => new EventRepositories(user).getAll());

    this.addQueryResolver<{ id: string }, IEvent | null>('getEvent',
      async (args, user) => new EventRepositories(user, args.id).getData());

    this.addMutationResolver<{ event: IEvent }, IEvent>('createEvent',
      async (args, user) => new EventRepositories(user).create(args.event));

    this.addMutationResolver<{ id: string, event: IEvent }, IEvent>('editEvent',
      async (args, user) => new EventRepositories(user, args.id).edit(args.event));

    this.addMutationResolver<{ id: string }, Boolean>('deleteEvent',
      async (args, user) => new EventRepositories(user, args.id).delete());

    this.addMutationResolver<{ eventId: string, userIds: string[] }, Boolean>('addParticipantsInEvent',
      async (args, user) => new EventRepositories(user, args.eventId).addParticipants(args.userIds));

    this.addMutationResolver<{ eventId: string, userIds: string[] }, Boolean>('removeParticipantsFromEvent',
      async (args, user) => new EventRepositories(user, args.eventId).removeParticipants(args.userIds));

    this.addEntityResolver<IEvent, IDomain | null>('Event', 'domain',
      async (event, user) => new EventRepositories(user, event).getDomain());

    this.addEntityResolver<IEvent, IUser[]>('Event', 'participants',
      async (event, user) => new EventRepositories(user, event.id).getParticipants());

    this.addEntityResolver<IEvent, IUser | null>('Event', 'facilitator',
      async (event, user) => new EventRepositories(user, event).getFacilitator());

  }
}

export const eventResolvers = new EventResolvers();