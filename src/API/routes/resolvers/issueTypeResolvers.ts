import { IssueTypeRepositories } from '../../../data/repositories/issueTypeRepositories';
import { IDomain } from '../../../types/IDomain';
import { IStage } from '../../../types/IStage';
import { IEntityId } from '../../../types/IEntityId';
import { IIssueType } from '../../../types/IIssueType';
import { BaseResolver } from './baseResolvers';

class IssueTypeResolvers extends BaseResolver {
  setResolvers() {
    this.setSchema(`
      type Query {
        getIssueTypesOfOrganization(domainId: ID!): [IssueType]!
        getIssueTypes: [IssueType]!
        getIssueType(id: ID!): IssueType!
      }

      type Mutation {
        createIssueType(issueType: IssueTypeInput!): IssueType
        editIssueType(id: ID!, issueType: IssueTypeInput!): IssueType
        deleteIssueType(id: ID!): Boolean
      }

      type IssueType {
        id: ID
        name: String
        description: String
        stages: [Stage]
        domain: Domain
        createdAt: DateTime
        updatedAt: DateTime
      }
      
      input IssueTypeInput {
          name: String
          description: String
          stageIds: [ID]
          domainId: ID
      }
    `);

    this.addQueryResolver<{ id: string }, IIssueType[]>('getIssueTypes',
      async (args, user) => new IssueTypeRepositories(user).getAll());

    this.addQueryResolver<{ domainId: string }, IIssueType[]>('getIssueTypesOfOrganization',
      async (args, user) => new IssueTypeRepositories(user).getOfOrganization(args.domainId));

    this.addQueryResolver<{ id: string }, IIssueType | null>('getIssueType',
      async (args, user) => new IssueTypeRepositories(user, args.id).getData());

    this.addMutationResolver<{ issueType: IIssueType }, IEntityId>('createIssueType',
      async (args, user) => new IssueTypeRepositories(user).create(args.issueType));

    this.addMutationResolver<{ id: string }, boolean>('deleteIssueType',
      async (args, user) => new IssueTypeRepositories(user, args.id).delete());

    this.addMutationResolver<{ id: string, issueType: IIssueType }, IIssueType>('editIssueType',
      async (args, user) => new IssueTypeRepositories(user, args.id).edit(args.issueType));

    this.addEntityResolver<IIssueType, IDomain | null>('IssueType', 'domain',
      async (issueType, user) => new IssueTypeRepositories(user, issueType.id).getDomain());

    this.addEntityResolver<IIssueType, IStage[]>('IssueType', 'stages',
      async (issueType, user) => new IssueTypeRepositories(user, issueType.id).getStages());
  }
}

export const issueTypeResolvers = new IssueTypeResolvers();