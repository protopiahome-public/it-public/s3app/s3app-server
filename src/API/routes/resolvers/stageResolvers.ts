import { StageRepositories } from '../../../data/repositories/stageRepositories';
import { IDomain } from '../../../types/IDomain';
import { IEntityId } from '../../../types/IEntityId';
import { IIssue } from '../../../types/IIssue';
import { IStage } from '../../../types/IStage';
import { BaseResolver } from './baseResolvers';

// export const stageResolvers = () => {

//   return {
//     Query: {
//       getStages: () => StageServices.getAll(),
//       getStage: async (_: object, args: { id: string }, context: { user: IStage }) => {
//         const stage = await StageServices.getData(args.id);
//         if (stage && !await DomainHierarchy.checkUserDomain(context.user.id, stage.domainId)) {
//           throw new Error('Access denied');
//         }
//         return stage;
//       },
//     },

//     Mutation: {
//       createStage: async (_: object, args: { stage: IStage }) => StageServices.create(args.stage),
//       editStage: async (_: object, args: { id: string, stage: IStage }) => StageServices.edit(args.id, args.stage),
//       deleteStage: async (_: object, args: { id: string }) => StageServices.delete(args.id),
//     },

//     Stage: {
//       domain: async (stage: IStage) => DomainServices.getData(stage.domainId),
//       issues: async (stage: IStage) => stage.id ? IssueServices.getByStage(stage.id) : null,
//     },
//   };
// };

class StageResolvers extends BaseResolver {
  setResolvers(): void {
    this.addQueryResolver<{}, IStage[]>('getStages', 
      (args, user) => new StageRepositories(user).getAll());

    this.addQueryResolver<{ id: string }, IStage | null>('getStage', 
      async (args, user) => new StageRepositories(user, args.id).getData());

    this.addMutationResolver<{ stage: IStage }, IEntityId>('createStage', 
      async (args, user) => new StageRepositories(user).create(args.stage));

    this.addMutationResolver<{ id: string, stage: IStage }, IStage>('editStage',
      async (args, user) => new StageRepositories(user, args.id).edit(args.stage));

    this.addMutationResolver<{ id: string }, boolean>('deleteStage',
      async (args, user) => new StageRepositories(user, args.id).delete());

    this.addMutationResolver<{ stageIds: string[] }, IStage[]>('orderStages',
      async (args, user) => new StageRepositories(user).order(args.stageIds));

    this.addEntityResolver<IStage, IDomain | null>('Stage', 'domain', 
      async (stage, user) => new StageRepositories(user, stage).getDomain());

    this.addEntityResolver<IStage, IIssue[]>('Stage', 'issues', 
      async (stage, user) => new StageRepositories(user, stage).getIssues());
  }
}

export const stageResolvers = new StageResolvers();