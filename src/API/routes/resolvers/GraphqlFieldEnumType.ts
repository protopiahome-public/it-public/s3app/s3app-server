import { GraphQLScalarType, Kind } from 'graphql';

function graphqlFieldEnumType(name: string, values: string[]) {
  return new GraphQLScalarType({
    name,
    description: 'Values: ' + values.join(', '),
    serialize(value) {
      if (typeof value === 'string' && values.includes(value)) {
        return value;
      }
      throw Error('Value must be one of: ' + values.join(', '));
    },
    parseValue(value) {
      if (typeof value === 'string' && values.includes(value)) {
        return value;
      }
      throw new Error('Value must be one of: ' + values.join(', '));
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.STRING && values.includes(ast.value)) {
        return ast.value;
      }
      throw new Error('Value must be one of: ' + values.join(', '));
    },
  });
}

export default graphqlFieldEnumType;